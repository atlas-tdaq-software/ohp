#include <iostream>

#include <ohp/CoreInterface.h>
#include <ohp/PluginBase.h>
#include <ohp/PluginManager.h>

#include <ohp/Configuration.h>
#include <ohp/ArchiveHistogramManager.h>
#include <ohp/FileHistogramManager.h>
#include <ohp/OnlineHistogramManager.h>
#include <ohp/PluginManager.h>
#include <ohp/FileManager.h>

#include <TProfile2D.h>
#include <TProfile3D.h>

#define ROOT_HISTOGRAM_TYPES(dimension, decl)		\
decl(dimension,TH 	## dimension ## C)	else	\
decl(dimension,TH 	## dimension ## S)	else	\
decl(dimension,TH 	## dimension ## I)	else	\
decl(dimension,TH 	## dimension ## F)	else	\
decl(dimension,TH 	## dimension ## D)	else	\
decl(dimension,TProfile ## dimension ## D)

#define TProfile1D TProfile

#define CLONE_HISTOGRAM(dimension,type)	\
	if(object->IsA() == type::Class()) { \
	    type* o = new type(*((type*)object)); \
            o->SetDirectory(0); \
            return o; \
	}

TObject*
ohp::CoreInterface::cloneObject(TObject* object)
{
   if(!object)
   	return 0;

   if(object->InheritsFrom(TH3::Class()))
   {
   	ROOT_HISTOGRAM_TYPES(3,CLONE_HISTOGRAM)
   }
   else if(object->InheritsFrom(TH2::Class()))
   {
   	ROOT_HISTOGRAM_TYPES(2,CLONE_HISTOGRAM)
   }
   else if(object->InheritsFrom(TH1::Class()))
   {
   	ROOT_HISTOGRAM_TYPES(1,CLONE_HISTOGRAM)
   }
   
   return object->Clone();
}

/**
 * Given the histogram complete name this method
 * return a pointer to struct containing: server name, provider name and histogram name
 * @param fullHistoName the full histogram name according to the naming convention
 * @returns server name, provider name and histogram name
 */
bool 
ohp::CoreInterface::tokenize(const std::string& fullHistoName, HistogramNameTokens & tokens)
{
    std::string::size_type p = fullHistoName.find('/');
    if (p == std::string::npos) {
	tokens.serverName = fullHistoName;
	return false;
    }

    tokens.serverName = fullHistoName.substr(0, p);
    if (tokens.serverName.empty()) {
	return false;
    }

    std::string::size_type h = fullHistoName.find('/', p + 1);
    if (h == std::string::npos) {
	tokens.providerName = fullHistoName.substr(p + 1);
	return false;
    }

    tokens.providerName = fullHistoName.substr(p + 1, h - (p + 1));
    if (tokens.providerName.empty()) {
	return false;
    }

    tokens.histogramName = fullHistoName.substr(h);
    tokens.oldHistogramName = fullHistoName.substr(h+1);
    
    return true;
}

ohp::CoreInterface & 
ohp::CoreInterface::getInstance(
	const std::string & partition_name, 
        const std::string& configuration_file)
{
    static CoreInterface* instance;
    
    if (!instance) {
    	instance = new CoreInterface(partition_name, configuration_file);
        instance->loadPlugins();
    }
    
    return *instance;
}

ohp::CoreInterface::CoreInterface(
	const std::string& partition_name, 
        const std::string& configuration_file)
  : m_configuration(new Configuration(partition_name,configuration_file)),
    m_histogram_manager(new OnlineHistogramManager(m_configuration->getPartition())),
    m_file_manager(new FileManager(m_configuration->getFiles())),
    m_plugin_manager(new PluginManager()),
    m_histogram_properties(new HistogramProperties()),
    m_infrastructure_checker(new InfrastructureChecker(	m_configuration->getPartition(),
    							m_configuration->getServers())),
    m_command_sender(m_configuration->getPartition())
{
    if (ers::debug_level()) {
    	m_configuration->dump(std::cout);
    }
}

void 
ohp::CoreInterface::infrastructureUp()
{
    m_histogram_manager->infrastructureUp();
    broadcastEvent(Event::InfrastructureUp);
}

void 
ohp::CoreInterface::infrastructureDown()
{
    m_histogram_manager->infrastructureDown();
    broadcastEvent(Event::InfrastructureDown);
}

bool 
ohp::CoreInterface::isOnline() const 
{ 
    return m_histogram_manager->isOnline();
}

void 
ohp::CoreInterface::serverUp(const std::string& name)
{
    broadcastEvent(Event::ServerUp, name);
}
	
void 
ohp::CoreInterface::serverDown(const std::string& name)
{
    broadcastEvent(Event::ServerDown, name);
}

void 
ohp::CoreInterface::getHistogramsList( 
	const std::string & server,
	const std::string & providers_regex,
	const std::string & histograms_regex,
	std::vector<HistogramNameTokens>& histograms) const
{
    m_histogram_manager->getHistogramsList(server, providers_regex, histograms_regex, histograms);
}

void
ohp::CoreInterface::getHistograms(Histograms& histograms) const
{
    m_histogram_manager->getHistograms(histograms);
}

void 
ohp::CoreInterface::registerPlugin(const std::string& key,
		    boost::function<PluginBase* (const std::string& )> creator)
{
    m_plugin_manager->registerPlugin(key, creator);
}

void 
ohp::CoreInterface::loadPlugins()
{
    m_plugin_manager->loadPlugins(m_configuration->getPlugins());
}

const ohp::Plugins& 
ohp::CoreInterface::getPlugins() const
{
    return m_plugin_manager->getPlugins();
}

boost::shared_ptr<ohp::PluginBase> 
ohp::CoreInterface::getMainPlugin() const
{
    return m_plugin_manager->getMainPlugin();
}

void 
ohp::CoreInterface::setHistogramSource(boost::shared_ptr<HistogramManager> manager)
{
    m_histogram_manager = manager;
    const Plugins& plugins = m_plugin_manager->getPlugins();
    Plugins::const_iterator it = plugins.begin();
    for(;it != plugins.end(); ++it) {
        it->second->histogramSourceChanged(); 
    }    
}

void 
ohp::CoreInterface::subscribe(const std::vector<std::string>& histogram_names, PluginBase& plugin)
{
    m_histogram_manager->subscribe(histogram_names, plugin);
}

unsigned int 
ohp::CoreInterface::getInNotifications() const
{
    return m_histogram_manager->getInNotifications();
}

unsigned int 
ohp::CoreInterface::getOutNotifications() const
{
    return m_histogram_manager->getOutNotifications();
}

void
ohp::CoreInterface::pause()
{
    m_histogram_manager->pause();
    broadcastEvent(Event::Paused);
}

void
ohp::CoreInterface::resume()
{
    m_histogram_manager->resume();
    broadcastEvent(Event::Resumed);
}

/**
 * @param servername Destination server name
 * @param prov Destination provider name
 * @param hname A single destination histogram
 * @param cmd The command
 */
bool 
ohp::CoreInterface::sendCommand(
	const std::string& histoCompleteName, 
        const std::string& cmd, 
        bool  to_provider)
{
    HistogramNameTokens tokens;
    CoreInterface::tokenize(histoCompleteName,tokens);

    if (to_provider){
	try {
	    m_command_sender.sendCommand(tokens.serverName,
		    tokens.providerName, cmd);
	}
	catch (daq::oh::ProviderNotFound & ex) {
	    ers::log(ex);
	    return false;
	}
    }
    else {
	try {
	    m_command_sender.sendCommand(tokens.serverName,
		    tokens.providerName, tokens.histogramName, cmd);
	}
	catch (daq::oh::ProviderNotFound & ex) {
	    try {
		m_command_sender.sendCommand(tokens.serverName,
			tokens.providerName, tokens.oldHistogramName, cmd);
	    }
	    catch (daq::oh::ProviderNotFound & ex) {
		ers::log(ex);
		return false;
	    }
        }
    }
    return true;
}

void 
ohp::CoreInterface::subscribe(const std::string& regex, PluginBase& plugin)
{
    m_histogram_manager->attach(regex, plugin);
}

void 
ohp::CoreInterface::unsubscribe(const std::string& regex, PluginBase& plugin)
{
    m_histogram_manager->detach(regex, plugin);
}

TObject* 
ohp::CoreInterface::getReferenceHistogram(
	const std::string& histogram, bool search_in_oh)
{
    TObject* h = m_file_manager->readHistogram(histogram,
            m_infrastructure_checker->getRunType());

    if (h == 0 && search_in_oh) {
    	Annotations a;
    	Time t;
	h = m_histogram_manager->getHistogram(histogram, a, t);
    }
    return h;
}

TObject* 
ohp::CoreInterface::getHistogram(
	const std::string& histogram, 
        Annotations& annotations,
        Time& time)
{
    return m_histogram_manager->getHistogram(histogram, annotations, time);
}

TObject* 
ohp::CoreInterface::getHistogram(const std::string& histogram)
{
    Annotations a;
    Time t;
    return m_histogram_manager->getHistogram(histogram, a, t);
}

const ohp::Configuration & 
ohp::CoreInterface::getConfiguration() const
{
    return *m_configuration;
}

void 
ohp::CoreInterface::setProperty(const std::string& histogram, 
				const std::string& key,
				const std::string& value)
{
    m_histogram_properties->setProperty(histogram, key, value);
}

void 
ohp::CoreInterface::setProperty(const std::string& histogram)
{
    m_histogram_properties->setProperty(histogram);
}

const ohp::Histogram::Properties *
ohp::CoreInterface::getHistogramProperties(const std::string& histogram) const
{
    return m_histogram_properties->getProperties(histogram);
}

IPCPartition 
ohp::CoreInterface::getPartition() const
{
    return m_configuration->getPartition();
}

void 
ohp::CoreInterface::broadcastEvent(Event::Type e, const std::string& data)
{
    const Plugins& plugins = m_plugin_manager->getPlugins();
    Plugins::const_iterator it = plugins.begin();
    for(;it != plugins.end(); ++it) {
        it->second->postEvent(new Event(e, data)); 
    }
}

const std::vector<std::string>& 
ohp::CoreInterface::getServers() const
{
    return m_configuration->getServers();
}

void 
ohp::CoreInterface::manageRootFile(const std::string& file_name, dqm_config::RunType rt)
{
    m_file_manager->addFile(file_name, rt);
}

const std::string &
ohp::CoreInterface::getHistogramDocumentation(const std::string& histogram) const
{
    static const std::string empty;
    
    const Histogram::Properties * properties = getHistogramProperties(histogram);
    if (!properties)
	return empty;

    return properties->findFirst("DOC");
}

uint32_t ohp::CoreInterface::getRunNumber() const {
    return m_histogram_manager->getRunNumber();
}

uint32_t ohp::CoreInterface::getLumiBlock() const {
    return m_histogram_manager->getLumiBlock();
}

