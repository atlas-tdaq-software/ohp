/*
 FILE:		OnlineHistogramManager.cpp
 AUTHOR:	Serguei Kolos
*/

#include <oh/OHIterator.h>

#include <ohp/OnlineHistogramManager.h>
#include <ohp/CoreInterface.h>
#include <ohp/PluginBase.h>

ohp::OnlineHistogramManager::Subscription::Subscription(
    const IPCPartition& partition,
    const std::string& regex,
    ohp::PluginBase& plugin,
    ohp::Cache& cache,
    unsigned int& in_cnt,
    unsigned int& out_cnt,
    bool& paused)
  : m_regex(regex),
    m_cache(cache),
    m_in_cnt(in_cnt),
    m_out_cnt(out_cnt),
    m_paused(paused),
    m_partition(partition)
{ 
    m_plugins.push_back(&plugin);
    subscribe();
}

void 
ohp::OnlineHistogramManager::Subscription::addPlugin(ohp::PluginBase& plugin)
{
    boost::mutex::scoped_lock lock(m_mutex);
    m_plugins.push_back(&plugin);
}

void 
ohp::OnlineHistogramManager::Subscription::subscribe()
{
    boost::mutex::scoped_lock lock(m_mutex);
    
    m_subscriber.reset(new OHSubscriber(m_partition, *this, false));
    HistogramNameTokens tokens;
    if (!CoreInterface::tokenize(m_regex,tokens)) {
	ERS_DEBUG(1, "Bad regular expression '"<<m_regex<<"' was given");
	throw daq::oh::InvalidRegex(ERS_HERE, m_regex);
    }
    
    m_subscriber->subscribe(tokens.serverName, 
    	OWLRegexp(tokens.providerName), OWLRegexp(tokens.oldHistogramName), false);
}

bool 
ohp::OnlineHistogramManager::Subscription::removePlugin(const ohp::PluginBase& plugin)
{
    boost::mutex::scoped_lock lock(m_mutex);
    m_plugins.erase(std::remove(m_plugins.begin(), m_plugins.end(), &plugin));
    return m_plugins.empty();
}

void 
ohp::OnlineHistogramManager::Subscription::objectChanged(
	const std::string& name, const OWLTime &, Reason reason)
{
    if (m_paused) {
    	return;
    }
    
    if (reason == Deleted) {
    	return;
    }
    
    std::string normalized_name = m_cache.objectChanged(name);
    
    boost::mutex::scoped_lock lock(m_mutex);
    ++m_in_cnt;
    for (size_t i = 0; i < m_plugins.size(); ++i) {
    	ERS_DEBUG(1, "Notifying '"<<m_plugins[i]->getName()<<"' plugin about '"
        		<<normalized_name<<"' histogram");
        m_plugins[i]->postEvent(new Event(Event::HistogramUpdated, normalized_name));
        ++m_out_cnt;
    }
}

//////////////////////////////////////////////////////////////////////////////
// class OnlineHistogramManager
//////////////////////////////////////////////////////////////////////////////
ohp::OnlineHistogramManager::OnlineHistogramManager(const IPCPartition& partition)
  : m_partition(partition),
    m_subscriber(new OHSubscriber(m_partition, *this, false)),
    m_rp_watcher(partition, "RunParams.RunParams",
            [this](const std::string & , const RunParams & rp) { m_run_number = rp.run_number; }),
    m_lb_watcher(partition, "RunParams.LumiBlock",
            [this](const std::string & , const LumiBlock & lb) { m_lumi_block = lb.LumiBlockNumber; })
{ ; }

void 
ohp::OnlineHistogramManager::infrastructureUp()
{
    ERS_DEBUG(0, "Restore histograms subscription");
    Subscriptions::iterator it = m_subscriptions.begin();
    for(  ; it != m_subscriptions.end(); ++it) {
    	watchHistogram(*it);
    }
    
    RegexSubscriptions::iterator rit = m_regex_subscriptions.begin();
    for ( ; rit != m_regex_subscriptions.end(); ++rit) {
    	(*rit)->subscribe();
    }
}

void 
ohp::OnlineHistogramManager::infrastructureDown()
{
    ERS_DEBUG(0, "Remove histograms subscription");
    m_subscriber.reset(new OHSubscriber(m_partition, *this, false));
}

void 
ohp::OnlineHistogramManager::attach(const std::string& regex, ohp::PluginBase& plugin)
{
    boost::mutex::scoped_lock lock(m_mutex);
    RegexSubscriptions::iterator it = m_regex_subscriptions.find(regex);
    if (it == m_regex_subscriptions.end()) {
    	try {
            m_regex_subscriptions.insert(
            	boost::shared_ptr<Subscription>(
                	new Subscription(m_partition, regex, plugin, m_cache, 
                        	m_in_cnt, m_out_cnt, m_paused)));
        }
        catch(daq::oh::Exception& ex) {
            ERS_LOG(ex);
        }
    }
    else {
    	(*it)->addPlugin(plugin);
    }
}
    
void 
ohp::OnlineHistogramManager::detach(const std::string& regex, ohp::PluginBase& plugin)
{
    boost::mutex::scoped_lock lock(m_mutex);
    RegexSubscriptions::iterator it = m_regex_subscriptions.find(regex);
    if (it != m_regex_subscriptions.end()) {
    	if ((*it)->removePlugin(plugin)) {
            m_regex_subscriptions.erase(it);
        }
    }
}

TObject* 
ohp::OnlineHistogramManager::getHistogram(
	const std::string& name, 
        Annotations& annotations, 
        Time& universal_time)
{
    boost::mutex::scoped_lock lock(m_mutex);

    if(m_subscriptions.find(name) == m_subscriptions.end()) {
	if (watchHistogram(name))
	    m_subscriptions.insert(name);
        else
            return 0;
    }

    ERS_DEBUG(1, "Reading '"<<name<<"' histogram from cache");
    return m_cache.getObject(name, annotations, universal_time);
}

void 
ohp::OnlineHistogramManager::getHistogramsList(
	const std::string & server, 
	const std::string & providers_regex, 
	const std::string & histograms_regex, 
        std::vector<HistogramNameTokens>& histograms) const
{
    try {
	ERS_DEBUG(0, "Exploring the '"<<server<<"' server");
	IPCPartition p = ohp::CoreInterface::getInstance().getPartition();

        OHIterator it(p, server, providers_regex, histograms_regex);
	histograms.reserve(it.entries());
        while (it()) {
	    histograms.push_back(HistogramNameTokens(server, it.provider(), it.name()));
	}
    }
    catch(daq::oh::Exception& ex) {
	ERS_DEBUG(0, ex);
    }
}

void 
ohp::OnlineHistogramManager::objectChanged(const std::string& name, const OWLTime &, Reason reason)
{
    if (reason == Deleted) {
    	return;
    }

    histogramUpdated(name);
}

bool 
ohp::OnlineHistogramManager::watchHistogram(const std::string& name)
{
    HistogramNameTokens tokens;
    if (!CoreInterface::tokenize(name,tokens)) {
	ERS_DEBUG(1, "Incomplete histogram name '"<<name<<"' was given");
	return false;
    }
    try{
	m_subscriber->subscribe(
	    tokens.serverName, tokens.providerName, tokens.histogramName, false);
	m_subscriber->subscribe(
	    tokens.serverName, tokens.providerName, tokens.oldHistogramName, false);
    }
    catch(daq::oh::Exception & ex) {
	ers::debug(ex, 1);
    }
    
    return true;
}

TObject* 
ohp::OnlineHistogramManager::readObject(
	const std::string& name, Annotations& annotations, Time& universal_time)
{
    HistogramNameTokens tokens;
    if (!CoreInterface::tokenize(name,tokens)) {
	ERS_DEBUG(1, "Incomplete histogram name '"<<name<<"' was given");
	return 0;
    }

    try {
	OHRootObject obj = OHRootReceiver::getRootObject(m_partition, 
        	tokens.serverName, tokens.providerName, tokens.histogramName);
	annotations = obj.annotations;
        universal_time = boost::posix_time::from_time_t(obj.time.c_time());
	TNamed* n = (TNamed*)obj.object.release();
	n->SetName(name.c_str());
	return n;
    } 
    catch (daq::oh::ObjectNotFound& ex) {
	try {
	    OHRootObject obj = OHRootReceiver::getRootObject(m_partition, 
            	tokens.serverName, tokens.providerName, tokens.oldHistogramName);
	    annotations = obj.annotations;
	    universal_time = boost::posix_time::from_time_t(obj.time.c_time());
	    TNamed* n = (TNamed*)obj.object.release();
            n->SetName(name.c_str());
	    return n;
	} 
        catch (daq::oh::Exception& ex) {
	    ers::debug(ex, 1);
	}
    } 
    catch (daq::oh::Exception& ex) {
	    ers::debug(ex, 1);
    }

    return 0;
}
