/*
 FILE:		Cache.cpp
 AUTHOR:	Serguei Kolos
*/

#include <oh/OHRootReceiver.h>

#include <ohp/Cache.h>
#include <ohp/CoreInterface.h>

namespace
{
    std::string 
    normalizedName(const std::string& name)
    {
	std::string provider(oh::util::get_provider_name(name));
	std::string histogram(oh::util::get_histogram_name(name));

	if (histogram[0] != '/')
	    histogram = "/" + histogram;

	return oh::util::get_server_name(name) + "/" + provider + histogram;
    }
}

void
ohp::Cache::getHistograms(Histograms& histograms) const
{
    boost::mutex::scoped_lock lock(m_mutex);
    
    histograms.resize(m_data.size());
    Data::const_iterator it = m_data.begin();
    Histograms::iterator r  = histograms.begin();
    for ( ; it != m_data.end(); ++it, ++r) {
    	*r = std::make_pair(it->m_name, it->m_object);
    }
}

std::string 
ohp::Cache::objectChanged(const std::string& name)
{
    std::string full_name = normalizedName(name);

    boost::mutex::scoped_lock lock(m_mutex);
    Data::iterator it = m_data.find(full_name);
    if (it != m_data.end()) {
	it->invalidate();
    }
    
    return full_name;
}

TObject* 
ohp::Cache::getObject(const std::string& name, Annotations& annotations, Time& universal_time)
{
    boost::mutex::scoped_lock lock(m_mutex);

    Data::iterator it = m_data.find(name);
    if (it == m_data.end()) {
	it = m_data.insert(Element(name)).first;
    }
    if (!it->m_up_to_date) {
	it->m_object.reset(m_object_reader(name, it->m_annotations, it->m_universal_time));
    }
    
    annotations = it->m_annotations;
    universal_time = it->m_universal_time;
    
    return CoreInterface::cloneObject(it->m_object.get());
}

void 
ohp::Cache::clear()
{
    boost::mutex::scoped_lock lock(m_mutex);
    m_data.clear();
}
