/*
 FILE:		ArchiveHistogramManager.cpp
 AUTHOR:	Serguei Kolos
*/

#include <boost/regex.hpp>

#include <mda/common/LB.h>

#include <ohp/ArchiveHistogramManager.h>
#include <ohp/CoreInterface.h>

TObject* 
ohp::ArchiveHistogramManager::Files::readObject(unsigned int run_number, const std::string& name)
{
    HistogramNameTokens tokens;
    if (!CoreInterface::tokenize(name,tokens)) {
	ERS_DEBUG(1, "Bad histogram name '"<<name<<"' is given");
	return 0;
    }

    try {
	std::set<daq::mda::HistoFile> hfiles;
        m_db->histogram(hfiles, ohp::CoreInterface::getInstance().getPartition().name(), 
        			run_number, tokens.serverName, 
                                tokens.providerName, tokens.histogramName);
	
        ERS_DEBUG(1, "Found " << hfiles.size() << " instances of the '" << name << "' histogram");
        
        if (hfiles.empty()) {
	    m_db->histogram(hfiles, ohp::CoreInterface::getInstance().getPartition().name(),
				    run_number, tokens.serverName,
				    tokens.providerName, tokens.oldHistogramName);
	    ERS_DEBUG(1, "Found " << hfiles.size() << " instances of the '" << name << "' histogram");
        }
        
        std::set<daq::mda::HistoFile>::iterator it = hfiles.begin();
        for ( ; it != hfiles.end(); ++it) {
	    if (it->lb() == daq::mda::LB::LastLB) {
                break;
            }
        }
        
        if (it == hfiles.end()) {
            return 0;
        }
        
	ERS_DEBUG(1, "Found EoR version of the '" << name << "' histogram");
        
        daq::mda::HistoFile hfile = *it;
        Cache::iterator ff = m_cache.find(hfile.file() + hfile.dataset());
        
        boost::shared_ptr<TFile> tfile;
        
        if (ff == m_cache.end()) {
	    tfile.reset(openFile(hfile.file(), hfile.dataset()));
            if (tfile) {
            	m_cache.insert(std::make_pair(hfile.file() + hfile.dataset(), tfile));
            }
            else {
		ERS_DEBUG(0, "File '" << hfile.file() << "' from the '"<< 
                	hfile.dataset() << "' is not found in archive");
            	return 0;
            }
        }
        else {
            tfile = ff->second;
        }
        
        TObject* o = tfile->Get(hfile.histoPath().c_str());
        return CoreInterface::cloneObject(o);
    } 
    catch (const ers::Issue& ex) {
        std::cerr << ex << std::endl;
    }
    
    return 0;
}

//////////////////////////////////////////////////////////////////////////////
// class ArchiveHistogramManager
//////////////////////////////////////////////////////////////////////////////
ohp::ArchiveHistogramManager::ArchiveHistogramManager(int number_of_runs)
  : m_db(new daq::mda::DBRead()),
    m_files(m_db),
    m_selected_run_number(-1)
{
    try {
        m_db->runs(m_runs, ohp::CoreInterface::getInstance().getPartition().name(), number_of_runs);
    } 
    catch (const ers::Issue& ex) {
        std::cerr << ex << std::endl;
        return;
    }

    m_run_numbers.reserve(m_runs.size());
    std::set<daq::mda::RunInfo>::const_iterator it = m_runs.begin();
    for ( ; it != m_runs.end(); ++it) {
        m_run_numbers.push_back(it->run());
    }    
}

void 
ohp::ArchiveHistogramManager::getHistogramsList(
	const std::string & server, 
	const std::string & providers_regex, 
	const std::string & histograms_regex, 
        std::vector<HistogramNameTokens>& histograms) const
{
    try {
	std::set<std::string> prs;

        m_db->ohsProviders(prs, ohp::CoreInterface::getInstance().getPartition().name(), 
				m_selected_run_number, server);
	
        ERS_DEBUG(1, "Found " << prs.size() << " providers");
        
        boost::regex pr(providers_regex);
        boost::regex hr(histograms_regex);
        
        std::set<std::string>::iterator it = prs.begin();
        for ( ; it != prs.end(); ++it) {
	    if (not boost::regex_match(*it, pr)) {
                continue;
            }
	    
            std::set<std::string> hrs;
	    ERS_DEBUG(1, "Searching histograms for '"<< *it << "' provider");
	    m_db->histoNames(hrs, ohp::CoreInterface::getInstance().getPartition().name(),
				    m_selected_run_number, server, *it);

	    ERS_DEBUG(1, "Found " << hrs.size() << " histograms for the '"<< *it << "' provider");
	    std::set<std::string>::iterator hit = hrs.begin();
	    for ( ; hit != hrs.end(); ++hit) {
		if (not boost::regex_match(*hit, hr)) {
		    continue;
		}
            	histograms.push_back(HistogramNameTokens(server, *it, *hit));
            }
        }
    }
    catch(daq::oh::Exception& ex) {
	ERS_DEBUG(0, ex);
    }
}

void
ohp::ArchiveHistogramManager::attach(const std::string& , ohp::PluginBase& )
{
}
    
void 
ohp::ArchiveHistogramManager::detach(const std::string& , ohp::PluginBase& )
{
}

void 
ohp::ArchiveHistogramManager::setRunNumber(unsigned int rn)
{
    std::set<daq::mda::RunInfo>::iterator it = m_runs.begin();
    for ( ; it != m_runs.end(); ++it ) {
    	if ( it->run() == rn ) {
	    m_selected_run_number = it->run();
	    m_selected_run_time = boost::posix_time::from_time_t(it->time());
	    reset();
	    m_files.clear();
            break;
        }
    }    
}

TObject* 
ohp::ArchiveHistogramManager::readObject(
	const std::string& name, Annotations& , Time& universal_time)
{
    universal_time = m_selected_run_time;
    return m_files.readObject(m_selected_run_number, name);
}
