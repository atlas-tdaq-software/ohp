// File:    HistogramProperties.cpp
// Author:  Serguei Kolos

#include <ers/ers.h>

#include <ohp/HistogramProperties.h>

ohp::Histogram::Properties::Properties(	
	const std::string & n, 
	const std::string& k, 
	const std::string& v)
  : m_name(n)
{ 
    addProperty(k,v);
}

void 
ohp::Histogram::Properties::addProperty(	
	const std::string& key,
	const std::string& value) const
{
    ERS_DEBUG(1, "Adding '"<<key<<"'='"<<value
    		<<"' pair to '"<<m_name<<" property");
    PropertiesMap::iterator it = m_properties.find(key);
    if (it != m_properties.end()) {
    	if (it->m_unique) {
            it->m_value = value;
            return ;
        }
    }
    m_properties.insert(Property(key,value));
}

const std::string &
ohp::Histogram::Properties::findFirst(const std::string & key) const
{
    static const std::string empty;
    PropertiesMap::iterator it = m_properties.find(key);
    if (it != m_properties.end()) {
    	return it->m_value;
    }
    else {
    	return empty;
    }
}

std::vector<std::string>
ohp::Histogram::Properties::findAll(const std::string& key) const
{
    std::vector<std::string> values;
    
    std::pair<PropertiesMap::iterator,PropertiesMap::iterator> range 
    		= m_properties.equal_range(key);
    for(PropertiesMap::iterator it = range.first; it != range.second; ++it) {
	values.push_back(it->m_value);
    }
    std::reverse(values.begin(), values.end());
    return values;
}

/**
 * If the histogram is not in the map, a new association will be created
 * otherwise the new property will be merged with the old ones, checking
 * if overwriting is enabled for this property
 *
 * @param histogram Histogram name
 * @param key The first parameter of the property
 * @param value The second parameter of the property
 */
void 
ohp::HistogramProperties::setProperty(
		const std::string& histogram,
		const std::string& key,
		const std::string& value)
{
    iterator it = find(histogram);
    if (it != end()) {
	it->addProperty(key,value);
    }
    else {
	insert(Histogram::Properties(histogram,key,value));
    }
}

void 
ohp::HistogramProperties::setProperty(const std::string& histogram)
{
    iterator it = find(histogram);
    if (it == end()) {
	insert(Histogram::Properties(histogram));
    }
}

const ohp::Histogram::Properties *
ohp::HistogramProperties::getProperties(const std::string& histogram) const
{
    iterator it = find(histogram);
    if (it != end()) {
	return &(*it);
    }
    else {
	return 0;
    }
}
