#include <algorithm>

#include <ohp/PluginInfo.h>

namespace
{
    bool toboolean(const std::string & s)
    {
	std::string v;
	std::transform(s.begin(), s.end(), v.begin(), ::tolower);
	return (v == "true");
    }
}

ohp::PluginInfo
ohp::PluginInfo::getInfo(const std::string& name) const
{
    Sections::const_iterator it = Section::m_subsections.get().find(name);
    if (it == Section::m_subsections.get().end())
	throw ohp::PluginInfoNotFound(ERS_HERE,name);
   
    return PluginInfo(*it);
}
  
bool 
ohp::PluginInfo::getParameter(const std::string& name, bool& value) const
{
    Options::const_iterator it = m_options.find(name);
    if (it == m_options.end())
	return false;

    value = toboolean(it->m_values[0]);

    return true;
}

bool 
ohp::PluginInfo::getParameters(const std::string& name, std::vector<bool>& values) const
{
    Options::const_iterator it = m_options.find(name);
    if (it == m_options.end())
	return false;
    
    values.resize(it->m_values.size());
    std::transform(it->m_values.begin(), it->m_values.end(),
		    values.begin(), toboolean);

    return true;
}

bool 
ohp::PluginInfo::getParameters(const std::string& name, std::vector<std::vector<std::string> >& values) const
{
    std::pair<Options::const_iterator,Options::const_iterator> range = 
    					m_options.equal_range(name);
    if (range.first == range.second)
	return false;
    
    for(Options::iterator it = range.first; it != range.second; ++it) {
    	values.push_back(it->m_values);
    }

    return true;
}
