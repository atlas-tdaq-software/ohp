/*
 FILE:		InfrastructureChecker.cpp
 AUTHOR:	Danilo Cimino [cimino@cli.di.unipi.it]
 MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Early 2008) -- Multiple server support added
 MODIFIED:	Serguei Kolos
 */
#include <algorithm>

#include <is/server.h>
#include <ohp/InfrastructureChecker.h>
#include <ohp/CoreInterface.h>
#include <ohp/Event.h>

ohp::InfrastructureChecker::InfrastructureChecker(
	const IPCPartition & partition, 
        const std::vector<std::string>& servers)
  : m_servers(servers),
    m_partition(partition),
    m_up_sent(true),
    m_down_sent(false),
    m_config_factory(partition.name()),
    m_alarm(5,boost::bind(&ohp::InfrastructureChecker::periodic_action,this))
{ ; }

bool 
ohp::InfrastructureChecker::periodic_action()
{
    if ( m_partition.isValid() ) {
        m_config_factory.updateRunParameters();

        for (	std::vector<std::string>::iterator it = m_down_servers.begin(); 
        	it != m_down_servers.end(); )
        {
	    if (is::server::exist(m_partition,*it)) {
		ERS_DEBUG( 3, "Server '"<<*it<<"' is up detected");
		CoreInterface::getInstance().serverUp(*it);
                it = m_down_servers.erase(it);
	    }
            else {
            	++it;
            }
	}
	
        for (	std::vector<std::string>::iterator it = m_servers.begin(); 
        	it != m_servers.end(); ++it )
        {
	    if (!is::server::exist(m_partition,*it)) {
		ERS_DEBUG( 3, "Server '"<<*it<<"' is down detected");
                if (std::find(m_down_servers.begin(),m_down_servers.end(),*it)==m_down_servers.end()) {
		    CoreInterface::getInstance().serverDown(*it);
		    m_down_servers.push_back(*it);
                }
	    }
	}
	
        if (m_down_servers.size()==m_servers.size())
        {
	    if (!m_down_sent) {
		ERS_DEBUG( 3, "Infrastructure is down detected");
            	CoreInterface::getInstance().infrastructureDown();
	    	m_down_sent=true;
	    	m_up_sent=false;
            }
        }
        else {
	    if (!m_up_sent) {
		ERS_DEBUG( 3, "Infrastructure is up detected");
		CoreInterface::getInstance().infrastructureUp();
		m_up_sent=true;
		m_down_sent=false;
	    }
	}
    }
    else
    {
	if (!m_down_sent)
	{
	    ERS_DEBUG( 3, "Infrastructure is down detected");
	    CoreInterface::getInstance().infrastructureDown();
	    for (   std::vector<std::string>::iterator it = m_servers.begin();
		    it != m_servers.end(); ++it )
	    {
		if (std::find(m_down_servers.begin(),m_down_servers.end(),*it)==m_down_servers.end()) {
		    CoreInterface::getInstance().serverDown(*it);
		    m_down_servers.push_back(*it);
		}
	    }
	    m_down_sent=true;
	    m_up_sent=false;
	}
    }
    
    return true;
}
