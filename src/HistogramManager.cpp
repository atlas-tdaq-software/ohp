/*
 FILE:		HistogramManager.cpp
 AUTHOR:	Serguei Kolos
*/

#include <boost/bind/bind.hpp>

#include <ohp/HistogramManager.h>

using namespace boost::placeholders;

//////////////////////////////////////////////////////////////////////////////
// class HistogramManager
//////////////////////////////////////////////////////////////////////////////
ohp::HistogramManager::HistogramManager()
  : m_cache(boost::bind(&ohp::HistogramManager::readObject, this, _1, _2, _3)),
    m_in_cnt(0),
    m_out_cnt(0),
    m_paused(false)
{ ; }

TObject* 
ohp::HistogramManager::getHistogram(
	const std::string& name, 
        Annotations& annotations, 
        Time& universal_time)
{
    return m_cache.getObject(name, annotations, universal_time);
}

void 
ohp::HistogramManager::subscribe(const std::vector<std::string>& histograms, 
					ohp::PluginBase& plugin)
{
    m_filter.subscribe(histograms, plugin);
}

void 
ohp::HistogramManager::histogramUpdated(const std::string& name)
{
    if (m_paused) {
    	return;
    }
    
    std::string normalized_name = m_cache.objectChanged(name);
    
    ++m_in_cnt;
    m_out_cnt += m_filter.histogramUpdated(normalized_name);
}

void 
ohp::HistogramManager::reset()
{
    m_cache.clear();
}
