#include <ohp/SharedLibrary.h>

ohp::SharedLibrary::SharedLibrary(const std::string & name, int flags)
  : m_name(name)
{
    ::dlerror(); //reset error
    m_handle = dlopen(name.c_str(), flags);
    if (!m_handle) {
	const char* e = ::dlerror();
	throw SharedLibraryException(ERS_HERE,m_name,e);
    }
}

ohp::SharedLibrary::~SharedLibrary()
{
    if (::dlclose(m_handle)) {
	const char* e = ::dlerror();
	ers::error(SharedLibraryException(ERS_HERE,m_name,e));
    }
}

void* 
ohp::SharedLibrary::findSymbol(const std::string& symbol)
{
    dlerror(); //reset error
    void* sym = dlsym(m_handle, symbol.c_str());
    const char* e = dlerror(); //get error
    if (e){
	throw ohp::SharedLibraryException(ERS_HERE,m_name,e);
    }
    return sym;
}
