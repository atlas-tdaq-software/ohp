/*
  FILE:         PluginManager.h
  AUTHOR:       Paolo Adragna [p.adragna@qmul.ac.uk]
  MODIFIED: 	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it]
  MODIFIED:	Serguei Kolos
*/

#include <algorithm>

#include <ers/ers.h>

#include <ohp/SharedLibrary.h>
#include <ohp/PluginBase.h>
#include <ohp/PluginManager.h>

boost::shared_ptr<ohp::PluginBase>
ohp::PluginManager::getPlugin(const std::string& name) const
{
    Plugins::const_iterator it = m_plugins.find(name);
    if (it == m_plugins.end())
    {
	throw ohp::PluginNotFound(ERS_HERE, name);
    }
    return it->second;
}

boost::shared_ptr<ohp::PluginBase> 
ohp::PluginManager::getMainPlugin() const
{
    if (!m_main_plugin) {
	throw ohp::PluginNotFound(ERS_HERE, "main");
    }
    return m_main_plugin;
}

boost::shared_ptr<ohp::PluginBase>
ohp::PluginManager::createPlugin(const PluginConfig& info)
{
    Libraries::iterator li = m_libraries.find(info.getLibrary());
    if (li == m_libraries.end()) {
    	boost::shared_ptr<ohp::SharedLibrary> library(
        	new ohp::SharedLibrary(info.getLibrary()));
    	m_libraries.insert(std::make_pair(info.getLibrary(),library));
    }
    
    Creators::iterator it = m_creators.find(info.getType());
    if (it != m_creators.end()) {
    	boost::shared_ptr<ohp::PluginBase> plugin(it->second(info.getName()));
	m_plugins.insert(std::make_pair(info.getName(),plugin));
        return plugin;
    }
    
    throw ohp::PluginCreatorNotFound(ERS_HERE, info.getType());
}

void 
ohp::PluginManager::registerPlugin(const std::string& key, 
		boost::function<PluginBase* (const std::string&)> creator) 
{
    Creators::iterator it = m_creators.find(key);
    if (it != m_creators.end()) {
    	ERS_LOG("'"<<key<<"' plugin creator is already registered");
    }
    m_creators[key] = creator;
    ERS_DEBUG(1, "'"<< key<<"' plugin creator was registered");
}

void 
ohp::PluginManager::loadPlugins(const std::vector<PluginConfig>& config)
{
    const PluginConfig* main_config = 0;
    
    for (std::vector<PluginConfig>::const_iterator it = config.begin(); 
    		it != config.end(); ++it) {
	try {
	    boost::shared_ptr<ohp::PluginBase> plugin = createPlugin(*it);
            if (plugin->getType() == ohp::MAIN) {
		m_main_plugin = plugin;
		main_config = &(*it);
            }
            else {
		plugin->configure(*it);		
            }
            ERS_DEBUG(1, "Plugin '"<<it->getName()<<"' was created");
            m_plugins.insert(std::make_pair(it->getName(),plugin));
        }
        catch(ers::Issue & ex) {
            ers::error(ex);
        }
    }
    
    if (!m_main_plugin) {
    	throw ohp::PluginNotFound(ERS_HERE, "main");
    }

    m_main_plugin->configure(*main_config);

    for ( Plugins::iterator it = m_plugins.begin(); 
    		it != m_plugins.end(); ++it) {
        if (it->second->getType() != ohp::MAIN) {
            it->second->aboutToDisplay();
        }
    }
    
    m_main_plugin->aboutToDisplay();
}
