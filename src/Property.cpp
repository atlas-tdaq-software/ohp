// File:    Property.cpp
// Info:    Implementation of the Property class
// Author:  Serguei Kolos

#include <ohp/Property.h>

namespace
{
    bool isUnique(const std::string& s) 
    {
	static const std::string Unique[] = { "REFONTOP", "REFERENCE", "DRAWOPT", "REFDRAWOPT", "REFNORM" };
	static const unsigned int size = 5;

	for (unsigned int i = 0; i < size; ++i) {
	    if (s == Unique[i]) {
		return true;
	    }
	}

	return false;
    }
}

ohp::Property::Property(const std::string& k, const std::string& v)
  : m_unique(isUnique(k)),
    m_key(k),
    m_value(v)
{ ; }
