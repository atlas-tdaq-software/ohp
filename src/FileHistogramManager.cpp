/*
 FILE:		FileHistogramManager.cpp
 AUTHOR:	Serguei Kolos
*/

#include <boost/filesystem/operations.hpp>

#include <TClass.h>
#include <TFile.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH1.h>
#include <TEfficiency.h>
#include <TKey.h>

#include <ohp/FileHistogramManager.h>
#include <ohp/CoreInterface.h>

//////////////////////////////////////////////////////////////////////////////
// class FileHistogramManager
//////////////////////////////////////////////////////////////////////////////
ohp::FileHistogramManager::FileHistogramManager(const std::string& file_name)
  : m_time(boost::posix_time::second_clock::universal_time()),
    m_file(TFile::Open(file_name.c_str()))
{    
    boost::filesystem::path path(file_name);
    if (boost::filesystem::exists(path)) {
	std::time_t t = boost::filesystem::last_write_time(path);
        m_time = boost::posix_time::from_time_t(t);

        try {
            std::string f = path.filename().string();
            if (f[0] == 'r') {
                auto p1 = f.find('_');
                m_run_number = std::stoi(f.substr(1, p1));
                if (f.size() > p1 and f[p1 + 1] == 'l') {
                    m_lumi_block = std::stoi(f.substr(p1 + 2, f.find('_', p1 + 2)));
                }
            }
        }
        catch (std::exception & e) {
        }
    }
}

void 
ohp::FileHistogramManager::attach(const std::string& , ohp::PluginBase& )
{
}
    
void 
ohp::FileHistogramManager::detach(const std::string& , ohp::PluginBase& )
{
}

TObject* 
ohp::FileHistogramManager::readObject(
	const std::string& name, Annotations& , Time& universal_time)
{
    if (!m_file)
    	return 0;
        
    TObject* h = m_file->Get(name.c_str());
    if (h) {
	ERS_DEBUG(2, "Found '"<<name<<"' histogram in the '"<<m_file->GetName()<<"' file");
	universal_time = m_time;
	return CoreInterface::cloneObject(h);
    }
    
    return 0;
}

void 
ohp::FileHistogramManager::getHistogramsList(
	const std::string & server_name, 
	const std::string & providers_regex, 
	const std::string & histograms_regex, 
        std::vector<HistogramNameTokens>& histograms) const
{
    if (!m_file)
    	return ;
        
    boost::regex pr(providers_regex);
    boost::regex hr(histograms_regex);

    TDirectory* server_folder = 0;
    
    TIter servers(m_file->GetListOfKeys());
    for (TKey* server = (TKey*)servers(); server; server = (TKey*)servers()) {
	if (server_name != server->GetName())
            continue ;
            
        TObject* o = m_file->Get(server->GetName());
	if (!o->IsA()->InheritsFrom(TDirectory::Class()))
            return ;
	
        server_folder = (TDirectory*)o;
        
        break;
    }
    
    if (not server_folder)
    	return ;
        
    TIter providers(server_folder->GetListOfKeys());
    for (TKey* provider = (TKey*)providers(); provider; provider = (TKey*)providers()) {

	TObject* o = server_folder->Get(provider->GetName());
	if (!o->IsA()->InheritsFrom(TDirectory::Class()))
	    continue ;

	if (not boost::regex_match(provider->GetName(), pr)) {
	    continue;
	}

	ERS_DEBUG(2, "Found '"<<provider->GetName()<<"' sub-folder in the '"<<server_name
			    <<"' server folder in the '"<<m_file->GetName()<<"' file");

	std::string folder("/");
        buildHistogramsList((TDirectory*)o, server_name, 
        	provider->GetName(), folder, hr, histograms);
    }
}

void 
ohp::FileHistogramManager::buildHistogramsList(
	TDirectory* provider,
        const std::string & server_name, 
        const std::string & provider_name, 
        const std::string & path_name, 
	const boost::regex & histograms_regex, 
        std::vector<HistogramNameTokens>& histograms) const
{
    TIter items(provider->GetListOfKeys());
    for (TKey* item = (TKey*)items(); item; item = (TKey*)items()) {
            
        TObject* o = provider->Get(item->GetName());
	if (o->IsA()->InheritsFrom(TDirectory::Class())) {
            buildHistogramsList((TDirectory*)o, server_name, provider_name, 
            		path_name + item->GetName() + "/", 
            		histograms_regex, histograms);
            continue;
	}
        
        if (    o->IsA()->InheritsFrom(TH1::Class())
             || o->IsA()->InheritsFrom(TGraph::Class())
             || o->IsA()->InheritsFrom(TGraph2D::Class())
             || o->IsA()->InheritsFrom(TEfficiency::Class()))
        {
            std::string name(path_name + item->GetName());
            if (boost::regex_match(name, histograms_regex)) {
		histograms.push_back(HistogramNameTokens(server_name, provider_name, name));
            }
	}
    }
}
