/*
  FILE:         SubscriptionFilter.cpp
  AUTHOR:       Paolo Adragna [p.adragna@qmul.ac.uk] 
  MODIFIED:	Serguei Kolos 
*/

#include <ers/ers.h>

#include <ohp/SubscriptionFilter.h>
#include <ohp/PluginManager.h>
#include <ohp/PluginBase.h>

ohp::SubscriptionFilter::Object::Object(const std::string & name, ohp::PluginBase& plugin)
  : m_name(name),
    m_plugin_name(plugin.getName()),
    m_plugin(plugin)
{
    ERS_DEBUG(2, "Got " << &m_plugin << " reference for the '" << m_plugin_name << "' plugin");
}

unsigned int 
ohp::SubscriptionFilter::histogramUpdated(const std::string & name)
{        
    std::shared_lock lock(m_mutex);
    
    unsigned int out_cnt = 0;
    std::pair<Objects::iterator,Objects::iterator> range=m_objects.get<0>().equal_range(name);
    for (Objects::iterator it=range.first; it!=range.second; ++it) {
	++out_cnt;
	ERS_DEBUG(3, "ohp::SubscriptionFilter::histogramUpdated: Notify '"<<it->m_plugin_name
        		<<"' about '"<<it->m_name<<"' histogram");
	it->m_plugin.postEvent(new Event(Event::HistogramUpdated, range.first->m_name));
    }
    
    return out_cnt;
}

void 
ohp::SubscriptionFilter::subscribe(
	const std::vector<std::string>& histogram_names, 
        ohp::PluginBase& plugin)
{
    std::unique_lock lock(m_mutex);

    if (histogram_names.empty()){
	ERS_DEBUG(1, "Pattern for the '"<<plugin.getName()<<"' is emptied");    
	std::pair<Plugins::iterator,Plugins::iterator> range=m_objects.get<1>().equal_range(plugin.getName());
	m_objects.get<1>().erase(range.first,range.second);
    }
    else{
	ERS_DEBUG(1, "Setting up new pattern for the '"<<plugin.getName()<<"' plugin");    
	std::vector<std::string>::const_iterator it  = histogram_names.begin();
	std::vector<std::string>::const_iterator end = histogram_names.end();
	for ( ; it != end; ++it ) {
	    m_objects.insert(Object(*it,plugin));
	}
    }
}
