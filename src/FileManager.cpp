/*
  FILE:		ohp::FileManager.h
  AUTHOR:	Danilo Cimino [cimino@cli.di.unipi.it]
  Modified:	Serguei Kolos
*/

#include <ohp/FileManager.h>
#include <ohp/CoreInterface.h>

#include <TFile.h>
#include <TObject.h>

ohp::FileManager::FileManager(const std::vector<std::pair<dqm_config::RunType, std::string>>& v)
{  
    for(auto it = v.begin(); it != v.end(); ++it) {
	addFile(it->second, it->first);
    }
}

void 
ohp::FileManager::addFile(const std::string& file_name, dqm_config::RunType rt)
{
    boost::mutex::scoped_lock lock(m_mutex);
    boost::shared_ptr<TFile> file(TFile::Open(file_name.c_str()));
    if (file) {
	m_files.push_back(std::make_pair(rt, file));
    }
}

TObject* 
ohp::FileManager::readHistogram(const std::string& name, dqm_config::RunType rt)
{
    boost::mutex::scoped_lock lock(m_mutex);
  
    for(auto it = m_files.begin(); it != m_files.end(); ++it) {
        if (rt != it->first) {
            continue;
        }
	TObject* h = it->second->Get(name.c_str());
	if (h) {
	    ERS_DEBUG(1, "Found '"<<name<<"' histogram in the '"
	            <<it->second->GetName()<<"' file");
            return CoreInterface::cloneObject(h);
	} else {
            ERS_DEBUG(1, "No '"<<name<<"' histogram was found in the '"
                    <<it->second->GetName()<<"' file");
	    return 0;
	}
    }

    for(auto it = m_files.begin(); it != m_files.end(); ++it) {
        if (it->first != dqm_config::Default) {
            continue;
        }
        TObject* h = it->second->Get(name.c_str());
        if (h) {
            ERS_DEBUG(1, "Found '"<<name<<"' histogram in the '"
                    <<it->second->GetName()<<"' file");
            return CoreInterface::cloneObject(h);
        } else {
            ERS_DEBUG(1, "No '"<<name<<"' histogram was found in the '"
                    <<it->second->GetName()<<"' file");
            return 0;
        }
    }

    ERS_DEBUG(1, "No '"<<name<<"' histogram was found in the known reference files");
    return 0;
}
