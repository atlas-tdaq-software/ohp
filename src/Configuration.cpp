/*
  FILE :   Configuration.cpp
  AUTHOR : Danilo Cimino [cimino@cli.di.unipi.it]
           Paolo Adragna [p.adragna@qmul.ac.uk]
           Andrea Dotti [andrea.dotti@pi.infn.it]
  MODIFIED : Serguei Kolos
*/

#include <fstream>
#include <algorithm>

#include <TFile.h>

#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>

#include <ohp/Configuration.h>
#include <ohp/CoreInterface.h>

namespace
{
    std::string makeSearchPath(char separator = ':')
    {
	std::string s(1, separator);
	char* p = getenv("TDAQ_OHP_PATH");
	return p ? s+p+s : s;
    }
    
    /** A function searching file in the search paths.
     *  Returns complete filepath if file is found.
     */
    std::string
    findFile(const std::string& file, char separator = ':')
    {
	static const std::string path = makeSearchPath(separator);
        
        std::string searchpath(path);
	for ( std::string::size_type pos = searchpath.find_first_of(separator);
	      pos != std::string::npos;
	      searchpath.erase(0,pos+1),
	      pos = searchpath.find_first_of(separator) )
	{
	    std::string testpath(searchpath,0,pos);
	    if (!testpath.empty() && testpath[testpath.size()-1]!='/') {
		testpath+='/';
	    }
	    std::string candidate = testpath + file;

	    FILE* f = fopen(candidate.c_str(),"r");
	    if (f) {
		fclose(f);
		return candidate;
	    }
	}

	return std::string();
    }
}

ohp::Configuration::Configuration(const std::string& partition_name, const std::string& file_name)
{
    readFromXML(partition_name,file_name);
}

void 
ohp::Configuration::addServer(const std::string& s)
{
    std::string server(replaceAll(s));
    
    if (   server.find('.') != std::string::npos 
    	|| server.find('*') != std::string::npos) {
        // a naive attempt to avoid adding regular expressions
        return ;
    }
    
    std::vector<std::string>::iterator it = std::find(
    			m_servers.begin(), m_servers.end(), server);
    if (it == m_servers.end())
    	m_servers.push_back(server);
}

void 
ohp::Configuration::addFile(const std::string& f, dqm_config::RunType rt)
{
    std::string file_name(replaceAll(f));
    
    boost::shared_ptr<TFile> file(TFile::Open(file_name.c_str()));
    if (not file) {
        throw ohp::BadConfiguration(ERS_HERE,
                "Cannot open '" + file_name + "' reference file");
    }

    auto it = std::find_if(m_files.begin(), m_files.end(), [&](const auto & pair){
        return pair.second == file_name and pair.first == rt;
    });

    if (it == m_files.end()) {
    	m_files.push_back(std::make_pair(rt, file_name));
    }
}

void
ohp::Configuration::dump(std::ostream & out) const
{
    out<<"PARTITION: "<<m_partition<<std::endl;
    
    out<<"SERVERS: "<<std::endl;
    for(size_t i = 0; i < m_servers.size(); ++i)
  	out<<m_servers[i]<<std::endl;
    
    out<<"REFERENCE FILES: "<<std::endl;
    for(size_t i = 0; i < m_files.size(); ++i) {
  	out<<"run_type: " << m_files[i].first<<" file: "<<m_files[i].second<<std::endl;
    }
    
    out<<"OPTIONS:"<<std::endl;
    Sections::iterator it  = m_options.begin();
    Sections::iterator end = m_options.end();
    for( ; it != end; ++it)
  	it->dump(out);
    
    out<<"GLOBAL OPTIONS:"<<std::endl;
    it  = m_global_options.begin();
    end = m_global_options.end();
    for( ; it != end; ++it)
  	it->dump(out);
    
    out<<"PLUGINS:"<<std::endl;
    for(size_t i = 0; i < m_plugins.size(); ++i)
  	m_plugins[i].dump(out);
}

void
ohp::Configuration::replaceVariables()
{
    if (m_vars.empty()) {
    	return ;
    }
    
    m_partition = replaceAll(m_partition);
    
    std::transform(m_servers.begin(), m_servers.end(), 
		   m_servers.begin(), boost::bind(&ohp::Configuration::replaceAll, this, _1));

    std::transform(m_files.begin(), m_files.end(), 
		   m_files.begin(),
		   [this](auto & pair){ replaceAll(pair.second); return pair; });
    
    
    Sections::iterator it  = m_options.begin();
    Sections::iterator end = m_options.end();
    for( ; it != end; ++it)
  	it->replaceVariables(boost::bind(&ohp::Configuration::replaceAll, this, _1));
    
    it  = m_global_options.begin();
    end = m_global_options.end();
    for( ; it != end; ++it)
  	it->replaceVariables(boost::bind(&ohp::Configuration::replaceAll, this, _1));
    
    for(size_t i = 0; i < m_plugins.size(); ++i)
  	m_plugins[i].replaceVariables(boost::bind(&ohp::Configuration::replaceAll, this, _1));
}

const ohp::Section* 
ohp::Configuration::getOptions(const std::string& tabname, const std::string& name) const
{
  Sections::const_iterator it = m_options.find(tabname + name);  
  return (it != m_options.end() ? &(*it) : 0);
}

const ohp::Section* 
ohp::Configuration::getGlobalOptions(const std::string& name) const
{
  Sections::const_iterator it = m_global_options.find(name);  
  return (it != m_global_options.end() ? &(*it) : 0);
}

std::vector<const ohp::Section*>
ohp::Configuration::getMatchedOptions(const std::string & name) const
{
    std::vector<const ohp::Section*> result;
    Sections::const_iterator it  = m_global_options.begin();
    Sections::const_iterator end = m_global_options.end();
    for(; it != end; ++it)
    {
	boost::regex regex(it->m_name);
	try {
	    if (boost::regex_match(name, regex)) {
		// result must be sorted by the lenght of the matched strings
		// this way most generic matches will go first, least generic - last
		std::vector<const ohp::Section*>::iterator r = result.begin();
		for ( ; r != result.end(); ++r) {
		    if ((*r)->m_name.size() > it->m_name.size()) {
			break;
		    }
		}
		result.insert(r, &(*it));
	    }
	}
        catch (boost::regex_error& e) { ; }
    }
    
    return result;
}

void
ohp::Configuration::readPluginOptions(PMXML_NODE node, PluginInfo& info)
{
    int n = mxml_get_number_of_children(node);
    for (int i = 0; i < n; ++i)
    {
	PMXML_NODE subnode = mxml_subnode(node, i);
        
	if (!mxml_get_value(subnode)) {
            PluginInfo pi(subnode->name);
	    readPluginOptions(subnode,pi);
            info.addSection(pi);
            continue;
	}
	
        char* multivalue = mxml_get_attribute(subnode,(char*)"is-multi-value");
	if (!multivalue) {
	    info.m_options.insert(Option(replaceAll(subnode->name),subnode->value));
	}
	else {
	    std::string mvs(multivalue);
	    std::transform(mvs.begin(),mvs.end(),mvs.begin(),::tolower);
	    if ( mvs=="yes" || mvs=="y" || mvs=="true" )
	    {		
                typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
		boost::char_separator<char> separators(" \t\n");
		const char* t = mxml_get_attribute(subnode,(char*)"token");
		if (t) {
		    separators = boost::char_separator<char>(t);
		}
                else {
		    t = mxml_get_attribute(subnode,(char*)"token-separator");
		    if (t) {
			separators = boost::char_separator<char>(t);
		    }
                }
		
		std::vector<std::string> values;
		std::string input(subnode->value);
		tokenizer tokens(input, separators);
		for (tokenizer::iterator it = tokens.begin(); it != tokens.end(); ++it)
		{
		    values.push_back(*it);
		}
		info.m_options.insert(Option(replaceAll(subnode->name),values));
	    }
	}
    }
}

void 
ohp::Configuration::readHistogramOptions(
	PMXML_NODE node, const std::string & tab_name, ohp::Sections & options)
{
    PMXML_NODE* histograms = 0;
    int histograms_num = mxml_find_nodes(node, (char*)"/histogram", &histograms);
    
    for(int i = 0; i < histograms_num; ++i)
    {
	std::string histogramName(histograms[i]->attribute_value[0]);
	HistogramNameTokens tokens;
	if (CoreInterface::tokenize(histogramName,tokens)) {
	    addServer(tokens.serverName);
        }

	std::string id(replaceAll(tab_name) + replaceAll(histogramName));
        Sections::iterator pos = options.find(id);
        if (pos == options.end()) {
            pos = options.insert(Section(id)).first;
        }
	PMXML_NODE* properties = 0;
	int properties_num = mxml_find_nodes(histograms[i], (char*)"/property", &properties);
	for(int j = 0; j < properties_num; ++j)
	{
	    std::string			k(replaceAll(properties[j]->attribute_name));
	    std::vector<std::string>	v(1,properties[j]->attribute_value[0]);
	    pos->m_options.insert(Option(k,v));
	}
	free(properties);
    }
    free(histograms);
}

std::string 
ohp::Configuration::replaceOne(const boost::xpressive::smatch& match) const
{
    Variables::const_iterator it = m_vars.find(match[1]);
    if (it != m_vars.end()) {
	ERS_DEBUG(1, "Variable '"<<match[1]<<"' will be replaced with '"<<it->m_value<<"'");
	return it->m_value;
    }
    ERS_DEBUG(1, "Variable '"<<match[1]<<"' is not found");
    return std::string();
}

std::string 
ohp::Configuration::replaceAll(const std::string& in) const
{
    using namespace boost;
    using namespace xpressive;
    
    static sregex re = "$(" >> (s1 = +_w) >> ")" | "${" >> (s1 = +_w) >> "}";
    
    boost::function<std::string (const boost::xpressive::smatch&)>
			f(bind(&ohp::Configuration::replaceOne, this, _1));
    
    std::string r;
    try {
	r = regex_replace(in, re, f);
    }
    catch (boost::xpressive::regex_error& ex) { ; }
    
    ERS_DEBUG(2, "String '"<<in<<"' was replaced with '"<<r<<"'");
    
    return r;
}

void 
ohp::Configuration::readFromXML(const std::string & partition_name, const std::string & file_name)
{  
    char error[4096];
    PMXML_NODE tree = mxml_parse_file((char*)file_name.c_str(), error, 4096);
    if(!tree) {
	throw ohp::BadConfiguration(ERS_HERE, error);
    }

    m_partition = partition_name;
    PMXML_NODE general = mxml_find_node(tree,(char*)"/general");
    if(general)
    {
        PMXML_NODE* vars = 0;
	int vars_num = mxml_find_nodes(general, (char*)"/variable", &vars);        
	for(int i = 0; i < vars_num; ++i)
	{
	    const char* name  = mxml_get_attribute(vars[i], (char*)"name");
	    const char* value = mxml_get_attribute(vars[i], (char*)"value");
            m_vars.insert(Variable(name, value));
        }
	free(vars);
        
	if (m_partition.empty()) {
	    PMXML_NODE node = mxml_find_node(general,(char*)"/partition");
	    if(node) {
		m_partition = node->attribute_value[0];
	    }
	    else {
		char* envp = getenv("TDAQ_PARTITION");
		if (envp) {
		    std::cerr << "WARNING: Partition name was not specified by command line or configuration file." << std::endl;
		    std::cerr << "	      Using the name specified in the TDAQ_PARTITION environment variable." << std::endl;

		    m_partition = envp;
		}
		else {
		    throw ohp::BadConfiguration(ERS_HERE,
				"Partition name is not specified in the command line "
				"nor in the configuration file "
				"nor by the TDAQ_PARTITION environment variable.");
		}
	    }
	}	
    }

    PMXML_NODE display = mxml_find_node(tree, (char*)"/display");
    if(display)
    {
        PMXML_NODE* tabs=0;
	int tabs_num = mxml_find_nodes(display, (char*)"/tab", &tabs);
	for(int i = 0; i < tabs_num; ++i)
	{
            readHistogramOptions(tabs[i],tabs[i]->attribute_value[0],m_options);
        }
	free(tabs);
    }
    
    PMXML_NODE globaloptions = mxml_find_node(tree,(char*)"/globaloptions");
    if(globaloptions)
    {
	readHistogramOptions(globaloptions,"",m_global_options);
    }
  
    PMXML_NODE reference = mxml_find_node(tree, (char*)"/reference");
    if(reference)
    {
	PMXML_NODE* nodes=0;
	int num = mxml_find_nodes(reference,(char*)"/source",&nodes);

	for(int i = 0; i < num; ++i)
	{
            const char* t = mxml_get_attribute(nodes[i], (char*)"type");
            std::string type (t ? t : "file:");

            const char* v = mxml_get_attribute(nodes[i], (char*)"value");
            if (not v) {
                throw ohp::BadConfiguration(
                        ERS_HERE, "No value is given by a reference source");
            }

            dqm_config::RunType run_type = dqm_config::Default;
            const char* rt = mxml_get_attribute(nodes[i], (char*)"run-type");
            if (rt) {
                std::string s(rt);
                bool found = false;
                for (size_t i = 0; i < std::size(dqm_config::RunTypeNames); ++i) {
                    if (boost::ifind_first(s, dqm_config::RunTypeNames[i])) {
                        run_type = (dqm_config::RunType)i;
                        found = true;
                        break;
                    }
                }
                if (not found) {
                    throw ohp::BadConfiguration(
                            ERS_HERE, "Invalid run type '"
                            + s + "' is used by a reference source");
                }
            }

	    if (type.find("file:") != std::string::npos){
		addFile(v, run_type);
	    }
	}
	free(nodes);
    }

    PMXML_NODE plugins_node = mxml_find_node(tree,(char*)"/plugins");
    if(plugins_node)
    {
        PMXML_NODE* plugins = 0;
	int plugins_num = mxml_find_nodes(plugins_node, (char*)"/plugin", &plugins);
	for(int i = 0; i < plugins_num; ++i)
	{
	    const char* name	= mxml_get_attribute(plugins[i], (char*)"name");
	    const char* type	= mxml_get_attribute(plugins[i], (char*)"type");
	    const char* library = mxml_get_attribute(plugins[i], (char*)"library");
	    
            if (!name)
            	throw ohp::BadConfiguration(ERS_HERE, "Plugin is missing name attribute");
            if (!type)
            	throw ohp::BadConfiguration(ERS_HERE, "'" + std::string(name) + "' plugin is missing type attribute");
            if (!library)
            	throw ohp::BadConfiguration(ERS_HERE, "'" + std::string(name) + "' plugin is missing library attribute");
	    
            m_plugins.push_back(PluginConfig(name, type, library));
            readPluginOptions(plugins[i],*(m_plugins.rbegin()));
	}
	free(plugins);
    }

    PMXML_NODE* includes=0;
    int num = mxml_find_nodes(tree, (char*)"/include", &includes);
    for(int i = 0; i < num; ++i)
    {
	if (!includes[i]->n_attributes)
        {
	    continue;
        }
	std::string short_name = includes[i]->attribute_value[0];
	
	std::string long_name = findFile(short_name);
	if (long_name.empty())
	{
	    throw ohp::BadConfiguration(ERS_HERE, "Included '" + short_name + "' file is not found");
	}
	readFromXML(m_partition,long_name);
    }
    free(includes);
    
    replaceVariables();
}
