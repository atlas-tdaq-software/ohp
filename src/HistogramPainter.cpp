/*
 FILE:		HistogramPainter.cpp
 AUTHOR:	Danilo Cimino [cimino@cli.di.unipi.it]
 MODIFIED:	Serguei Kolos
 */
#include <algorithm>

#include <TEfficiency.h>
#include <TH1.h>
#include <TColor.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TStyle.h>
#include <TLatex.h>
#include <TText.h>
#include <TPaveText.h>
#include <TROOT.h>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>
#include <boost/algorithm/string.hpp>

#include <dqmf/is/Result.h>

#include <ohp/HistogramPainter.h>

#include <ohp/Configuration.h>
#include <ohp/CoreInterface.h>
#include <ohp/HistogramProperties.h>

namespace
{    
    std::string nexttoken(std::string& thestring)
    {
	const char token = ',';
	std::string::size_type pos = thestring.find(token);
	std::string previous;
	if (pos != std::string::npos) {
	    previous = std::string(thestring, 0, pos);
	    thestring.erase(0, pos + 1);
	} else {
	    previous = thestring;
	    thestring.erase();
	}
	return boost::trim_copy(previous);
    }
    
    int 
    execute(TObject * object, const std::string& call)
    {
	int error = 0;
	std::string::size_type start = call.find( '(' );
	if ( start != std::string::npos )
	{
	    std::string method = call.substr( 0, start );
	    std::string::size_type end = call.rfind( ')' );
	    
            std::string args;
	    if ( end != std::string::npos )
		args = call.substr( start + 1, end - start - 1 );
            
            std::replace(args.begin(), args.end(), '\'', '\"');
            
            ERS_DEBUG(1, "Executing '"<<method<<"("<<args<<")' function");
            object -> Execute(method.c_str(), args.c_str(), &error);
            if (error) {
            	ERS_LOG( "Got "<<error<<" error while executing '"<<method<<"("<<args<<")' function");
            }
	}
        return error;
    }
    
    TLine *
    makeLine(const std::string & p) {
	TLine* line = 0;
        std::string params(p);
	try {
	    Double_t x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	    Color_t color = 1;
	    Width_t width = 1;
	    Style_t style = 1;

	    x1 = boost::lexical_cast<Double_t>(nexttoken(params));
	    y1 = boost::lexical_cast<Double_t>(nexttoken(params));
	    x2 = boost::lexical_cast<Double_t>(nexttoken(params));
	    y2 = boost::lexical_cast<Double_t>(nexttoken(params));
	    if (!params.empty())
		    color = boost::lexical_cast<Color_t>(nexttoken(params));
	    if (!params.empty())
		    style = boost::lexical_cast<Width_t>(nexttoken(params));
	    if (!params.empty())
		    width = boost::lexical_cast<Style_t>(nexttoken(params));

	    line = new TLine(x1, y1, x2, y2);
	    line->SetBit(kCanDelete);
	    line->SetLineColor(color);
	    line->SetLineStyle(style);
	    line->SetLineWidth(width);
        }
	catch(boost::bad_lexical_cast & ex) {
	    ERS_LOG("Received exception: " << ex.what());
	    ERS_LOG("Parsing line '" + p + "' fails at '"<< params << "'");
	}
        return line;
    }
    
    void drawText(const std::string & text, bool NDC)
    {
	std::string txt, token;
	double xCoord = 0, yCoord = 0;
	std::istringstream iss(text), curTok;
	unsigned cnt = 0;

	while (getline(iss, token, ':')) {
	    curTok.clear();
	    curTok.str(token);
	    switch (cnt) {
		case 0:
		    txt = token;
		    cnt++;
		    break;
		case 1:
		    curTok >> xCoord;
		    cnt++;
		    break;
		case 2:
		    curTok >> yCoord;
		    break;
		default:
		    break;
	    }
	}

	if (cnt == 2) {
	    TLatex label;
            label.SetNDC(NDC);
	    ERS_DEBUG(2, "Painting '"<<txt<<"' text at "<<xCoord<<":"<<yCoord);
            label.DrawLatex(xCoord, yCoord, txt.c_str());
	}
    }
    
    void drawErrorMessage(const std::string & histogram)
    {
	TPaveText* text = new TPaveText(0.03, 0.03, 0.97, 0.97, "NDC");
	text->SetBit(kCanDelete);
	text->SetFillColor(TColor::GetColor(255, 255, 222)); // yellow
	
	std::string name(histogram);
        std::string::size_type pos;
	double x=0.,y=0.9;
        while((pos = name.find( '/' )) != std::string::npos){
	    std::string token = name.substr(0,pos+1);
	    name.erase(0,pos+1);
	    TText* t = text->AddText(x,y,token.c_str());
	    t->SetTextSize(0.07);
	    t->SetTextAlign(11); //left-bottom
	    t->SetTextFont(42);
	    t->SetTextColor(TColor::GetColor(0, 0, 204)); // blue
	    y-=0.09;
        }
        
	TText* t = text->AddText(x+0.1,y,name.c_str());
	t->SetTextSize(0.07);
	t->SetTextAlign(11);	//left-bottom
	t->SetTextFont(42);
	t->SetTextColor(1);
	y-=0.09;
	
        t = text->AddText(x,y,"is not found");
	t->SetTextSize(0.07);
	t->SetTextAlign(21);	// center-bottom
	t->SetTextFont(52);
	t->SetTextColor(TColor::GetColor(204, 102, 0)); //orange

	text->Draw();
    }

    void drawTime(const ohp::Time& time)
    {
        std::string s(boost::posix_time::to_simple_string(
        		boost::date_time::c_local_adjustor<boost::posix_time::ptime>::utc_to_local(time)));

        uint32_t rn = ohp::CoreInterface::getInstance().getRunNumber();
        if (rn != uint32_t(-1)) {
            s += " rn=" + std::to_string(rn);
            uint32_t lb = ohp::CoreInterface::getInstance().getLumiBlock();
            if (lb != uint32_t(-1)) {
                s += " lb=" + std::to_string(lb);
            }
        }

	TText t;
	t.SetNDC(kTRUE);
        t.SetTextAlign(11);
        t.SetTextColor(kYellow+3);
	t.SetTextFont(42);
	t.SetTextSize(0.04);
	t.DrawText(0.04, 0.01, s.c_str());
    }
}

bool 
ohp::histogram::drawWithConfig(
	const std::string& tab, const std::string& h, TPad* pad, const std::string& options)
{
    const Histogram::Properties * properties = CoreInterface::getInstance().getHistogramProperties(h);
    if (!properties) {
        CoreInterface::getInstance().setProperty(h);
        
        std::vector<const ohp::Section*> opts = 
        	CoreInterface::getInstance().getConfiguration().getMatchedOptions(h);
                
	for ( std::vector<const ohp::Section*>::const_iterator it = opts.begin();
		it != opts.end(); ++it )
	{
	    const ohp::Section* b = *it;
	    ERS_DEBUG(2, "Processing section '"<<b->m_name<<"' for the '"<<h<<"' histogram");
	    Options::const_iterator bit = b->m_options.begin();
	    Options::const_iterator end = b->m_options.end();
	    for ( ; bit != end; ++bit ) {
		CoreInterface::getInstance().setProperty(h, bit->m_key, bit->m_values[0]);
	    }
	}

	if (!tab.empty()) {
	    const Section* opts = 
            	CoreInterface::getInstance().getConfiguration().getOptions(tab, h);
	    if (opts) {
		ERS_DEBUG(2, "Processing section '"<<opts->m_name<<"' for the '"<<tab<<"' tab");
		Options::const_iterator it  = opts->m_options.begin();
		Options::const_iterator end = opts->m_options.end();
		for ( ; it != end; ++it ) {
		    CoreInterface::getInstance().setProperty(h, it->m_key, it->m_values[0]);
		}
	    }
	}

	properties = CoreInterface::getInstance().getHistogramProperties(h);
       	std::vector<std::string> styles = properties->findAll("STYLE");
        
        std::string style_name(styles.empty() ? "ATLAS" : h);
        TStyle* style = gROOT->GetStyle(style_name.c_str());
        
        if (!styles.empty() && !style) {
	    ERS_DEBUG(2, "Setting up '"<<style_name<<"' style for the '"<<h<<"' histogram");
            
	    TStyle* style = new TStyle(style_name.c_str(), style_name.c_str());
            
            // SetPalette must be called each time new style is created
            // as the TStyle constructor always resets the global Palette
            // variable to its default value which is not the same as in 
            // the ATLAS style
	    gROOT->GetStyle("ATLAS")->Copy(*style);
	    gROOT->GetStyle("ATLAS")->SetPalette(1,0);

	    std::vector<std::string>::const_iterator it;
	    for (it = styles.begin(); it != styles.end(); ++it) {
		execute(style,*it);
	    }
	}
        else {
            if (!style) {
            	style_name = "ATLAS";
            }
        }
        CoreInterface::getInstance().setProperty(h, "style_name", style_name);
    }

    std::string style_name = properties->findFirst("style_name");
    gROOT->SetStyle(style_name.c_str());
    ERS_DEBUG(2, "Using '"<<style_name<<"' style for the '"<<h<<"' histogram");
    
    pad->ResetAttPad();
    pad->UseCurrentStyle();
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Read histogram
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ohp::Annotations annotations;
    ohp::Time time;
    bool isHistogram = false;
    bool isRefHistogram = false;
    TObject* href  = 0;
    TObject* histo = CoreInterface::getInstance().getHistogram(h, annotations, time);

    if (!histo) {
        drawErrorMessage(h);
	pad->cd(0);
	pad->Draw();
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Setup pad for Reference histogram
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::string rname = properties->findFirst("REFERENCE");
    if (rname.empty()) {
	rname = h;
    }
    bool refontop = true;
    href = CoreInterface::getInstance().getReferenceHistogram(rname, rname!=h);
    if (href) {
	href->SetBit(kCanDelete);
	TH1* refHisto = dynamic_cast<TH1*> (href);
	if (refHisto) {
	    refHisto->SetStats(false);
	    isRefHistogram = true;
	}
	ERS_DEBUG(1, "Found reference for the '"<<h<<"' histogram");
    }
    std::string s = properties->findFirst("REFONTOP");
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    if (href && (s == "" || s == "false" || s == "no")) {
	refontop = false;
    }
    ERS_DEBUG(1, "REFONTOP is set to "<<refontop<<" for the '"<<h<<"' histogram");

    if (!refontop) {
	pad->Divide(1, 2);
	pad->cd(1);
    }
    else {
	pad->cd(0);
    }

    histo->SetBit(kCanDelete);

    if (histo->IsA()->InheritsFrom("TH1")) {
	isHistogram = true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Copy Style options from gStyle to the histogram
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    histo->UseCurrentStyle();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Draw options
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::string drawOpt = properties->findFirst("DRAWOPT");
    drawOpt += options;

    ohp::Annotations::iterator it;
    ohp::Annotations::iterator end = annotations.end();
    for (it = annotations.begin(); it != end; ++it) {
	CoreInterface::getInstance().setProperty(h, (*it).first, (*it).second);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Histogram properties
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::vector<std::string> methods = properties->findAll("HISTO");
    std::vector<std::string>::const_iterator methodIter;
    std::vector<std::string>::const_iterator methodEnd = methods.end();
    for (methodIter = methods.begin(); methodIter != methodEnd; ++methodIter) {
	if (!isHistogram) {
            continue;
	}
	if (*methodIter == "NORM") {
	    double i = ((TH1*)histo)->GetEntries();
            if (i != 0.) {
	    	((TH1*)histo)->Scale(1.0 / i);
            }
	}
	else if (*methodIter == "NORMINTEGRAL") {
	    double i = ((TH1*)histo)->Integral();
            if (i != 0.) {
	    	((TH1*)histo)->Scale(1.0 / i);
            }
	}
	else {
	    execute(histo, *methodIter);
	}
    }

    std::string title = properties->findFirst("TITLE");
    if (!title.empty()) {
	((TNamed*)histo)->SetTitle(title.c_str());
    }

    if (CoreInterface::getInstance().isOnline())
    {
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// DQMF
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	std::string dqmf = properties->findFirst("DQMF");
	if (!dqmf.empty()) {
	    ERS_DEBUG(2, "Processing '"<<dqmf<<"' DQMF status for '"<<h<<"' histogram");

	    try {
		ISInfoDictionary dict(CoreInterface::getInstance().getPartition());
		dqmf::is::Result res;
		dict.getValue(dqmf, res);
		if (res.status == dqmf::is::Result::Red) {
		    pad->SetFillColor(kRed);
		} else if (res.status == dqmf::is::Result::Yellow) {
		    pad->SetFillColor(kYellow);
		} else if (res.status == dqmf::is::Result::Green) {
		    pad->SetFillColor(kGreen);
		}
	    }
	    catch (daq::is::Exception & ex) {
		ERS_DEBUG(2, "Error in retrieving DQMF status:" << ex);
	    }
	}
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Axis properties
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for (int i = 0; i < 3; i++) {
	std::string propName(1, 'X' + i);
	propName += "AXIS";
	std::vector<std::string> property = properties->findAll(propName);

	for (std::vector<std::string>::iterator it = property.begin(); it != property.end(); it++) {
	    switch (i) {
	    case 0:
		if (isHistogram)
		    execute(static_cast<TH1*>(histo)->GetXaxis(), *it);
		else if (histo->IsA()->InheritsFrom("TGraph"))
		    execute(static_cast<TGraph*>(histo)->GetXaxis(), *it);
		break;
	    case 1:
		if (isHistogram)
		    execute(static_cast<TH1*>(histo)->GetYaxis(), *it);
		else if (histo->IsA()->InheritsFrom("TGraph2D"))
		    execute(static_cast<TGraph2D*>(histo)->GetYaxis(), *it);
		break;
	    case 2:
		if (isHistogram)
		    execute(static_cast<TH1*>(histo)->GetZaxis(), *it);
	    default:
		    break;
	    }
	}
    }

    histo->Draw(drawOpt.c_str());

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Texts to be drawn
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    {
	std::vector<std::string> texts = properties->findAll("TEXT");
	std::vector<std::string>::const_iterator textsIt;
	std::vector<std::string>::const_iterator textsEnd = texts.end();
	for (textsIt = texts.begin(); textsIt != textsEnd; ++textsIt) {
	    drawText(*textsIt,false);
	}
    }
    {
	std::vector<std::string> texts = properties->findAll("TEXTNDC");
	std::vector<std::string>::const_iterator textsIt;
	std::vector<std::string>::const_iterator textsEnd = texts.end();
	for (textsIt = texts.begin(); textsIt != textsEnd; ++textsIt) {
	    drawText(*textsIt,true);
	}
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lines to be drawn
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::vector<std::string> lines = properties->findAll("LINENDC");

    std::vector<std::string>::const_iterator linesIt;
    std::vector<std::string>::const_iterator linesEnd = lines.end();
    for (linesIt = lines.begin(); linesIt != linesEnd; ++linesIt) {
	TLine * line = makeLine(*linesIt);
	if (line){
	    line->SetBit(TLine::kLineNDC);
	    line->Draw();
	}
    }

    lines = properties->findAll("LINE");

    linesEnd = lines.end();
    for (linesIt = lines.begin(); linesIt != linesEnd; ++linesIt) {
	TLine * line = makeLine(*linesIt);
	if (line){
	    line->Draw();
	}
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Draw time
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    drawTime(time);
        
    if (!href) {
	pad->cd(0);
	pad->Draw();
	return true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Draw Reference histogram
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::string refNorm = properties->findFirst("REFNORM");

    if ( histo && isRefHistogram && isHistogram && !refNorm.empty() ) {
	TH1* hrefcopy = (TH1*) CoreInterface::getInstance().cloneObject(href);
	hrefcopy->SetBit(kCanDelete);
	Double_t i = hrefcopy->GetEntries();
	Double_t i2 = ((TH1*) histo)->GetEntries();
	if ( boost::iequals(refNorm, "integral") ) {
	    i = hrefcopy->Integral();
	    i2 = ((TH1*) histo)->Integral();
	} 
        if (i != 0 && i2 != 0) {
	    hrefcopy->Scale(i2 / i);
        }
	href = hrefcopy;
    }

    std::string refDrawOpt = properties->findFirst("REFDRAWOPT");

    //Get reference histogram line color/style
    Color_t hrefcol = 6;
    Style_t hrefsty = 12;
    Width_t hrefwid = 3;

    std::string reflin = properties->findFirst("REFCOL");
    if (!reflin.empty()) {
	short colidx;
	std::istringstream refcoli(reflin);
	if (refcoli >> colidx) {
	    hrefcol = (Color_t) colidx;
	}
    }

    reflin = properties->findFirst("REFSTY");

    if (!reflin.empty()) {
	short colidx;
	std::istringstream refcoli(reflin);
	if (refcoli >> colidx) {
	    hrefsty = (Style_t) colidx;
	}
    }

    reflin = properties->findFirst("REFWID");

    if (!reflin.empty()) {
	short colidx;
	std::istringstream refcoli(reflin);
	if (refcoli >> colidx) {
	    hrefwid = (Width_t) colidx;
	}
    }

    if (refontop) {
	TAttLine * l=dynamic_cast<TAttLine*>(href);
	if (l) {
	    l->SetLineColor(hrefcol);
	    l->SetLineStyle(hrefsty);
	    l->SetLineWidth(hrefwid);
	}

        TAttMarker* a = dynamic_cast<TAttMarker*>(href);
        if (a) {
            a-> SetMarkerColor(hrefcol);
        }

	std::string newOpt(refDrawOpt + "same");
	href->Draw(newOpt.c_str());
    }
    else {
	pad->cd(2);
	href->Draw(refDrawOpt.c_str());
    }

    pad->cd(0);
    pad->Draw();
    return true;
}

void 
ohp::histogram::draw(const std::string& h, TPad* pad, const std::string& options)
{
    gROOT->SetStyle("ATLAS");
    pad->UseCurrentStyle();

    TObject* histo = CoreInterface::getInstance().getHistogram(h);

    if (!histo) {
	drawErrorMessage(h);
    }
    else {
	histo->SetBit(kCanDelete);

	if (histo->IsA()->InheritsFrom("TH1"))
	    histo->Draw(options.c_str());
	else if (histo->IsA()->InheritsFrom("TGraph"))
	    histo->Draw("A*");
        else if (histo->IsA()->InheritsFrom("TGraph2D"))
            histo->Draw("A*");
    }

    pad->Draw();
}
