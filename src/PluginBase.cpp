/*
 FILE:		PluginBase.cpp
 AUTHOR:	Andrea Dotti [andrea.dotti@pi.infn.it]
 MODIFIED:	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
 MODIFIED:	Serguei Kolos
 */

#include <QApplication>
#include <QWidget>

#include <ohp/PluginBase.h>
#include <ohp/PluginManager.h>
#include <ohp/PluginInfo.h>
#include <ohp/CoreInterface.h>

ohp::PluginBase::PluginBase(const std::string& name, PluginType type, QWidget* widget)
  : m_name(name),
    m_type(type), 
    m_widget(widget)
{ ; }

ohp::PluginBase::~PluginBase() 
{ ; }

void 
ohp::PluginBase::setDoc(const std::string& doc)
{
    m_doc = doc;
}

void 
ohp::PluginBase::setWidget(QWidget* widget)
{
    m_widget = widget;
}

void 
ohp::PluginBase::subscribe(const std::vector<std::string>& histogram_names)
{
    ohp::CoreInterface::getInstance().subscribe(histogram_names, *this);
}

void 
ohp::PluginBase::postEvent(QEvent* e) 
{ 
    QApplication::postEvent(m_widget, e);
}
