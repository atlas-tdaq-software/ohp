/*
 FILE:		HistogramPainter.cpp
 AUTHOR:	Serguei Kolos
 */

#include <ohp/Section.h>

void 
ohp::Section::replaceVariables(boost::function<std::string (const std::string&)> op) const
{
    m_name = op(m_id);
    
    for (Options::iterator it = m_options.begin(); it != m_options.end(); ++it)
	it->replaceVariables(op);
        
    for (Sections::iterator it = m_subsections.get().begin(); 
    			    it != m_subsections.get().end(); ++it)
	it->replaceVariables(op);
}

void
ohp::Section::dump(std::ostream & out) const
{
    out << m_name << " { " << std::endl;
    
    Options::iterator it  = m_options.begin();
    Options::iterator end = m_options.end();
    for( ; it != end; ++it)
    {
	out << " ( " + it->m_key << ", ";
	out << "[ ";
	for(size_t i = 0; i < it->m_values.size(); ++i )
	{
	  out << "<" << it->m_values[i] + "> ";
	}
	out << "] )" << std::endl;
    }
    
    for (Sections::iterator it = m_subsections.get().begin(); 
    		it != m_subsections.get().end(); ++it) 
    {
	it->dump(out);
    }

    out << "}" << std::endl;
}


