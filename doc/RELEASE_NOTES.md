# ohp

* OHP configuration has been extended to support run type dependent reference files. This modification is fully backward compatible.
* Behavior of OHP has been changed if a wrong reference file is defined in the configuration. Previously such an problem was ignored. In the current release OHP prints error message and exits.

### Run type specific references

A new attribute called **run-type** has been added to the **source** XML tag, which can be used to define a type of runs a particular reference file should be used for. Valid values of this attribute are:

* **PP** - proton-proton physics runs
* **HI** - heavy ion physics runs
* **Cosmic** - comics runs
* **Default** - any run type for which no reference file is explicitly defined

Note that comparison is case-insensitive, so any combination of lower and upper case letters, like for example **pp** or **pP** would be equally valid. For backward compatibility if **run-type** attribute is not present then **Default** value will be used.

One can declare the **source** XML tag multiple times with different values of **run-type** attribute, in which case OHP will choose an appropriate one depending on the current run type. If no suitable reference file is defined for the current run type, then no reference histograms will be displayed. It's therefore strongly recommended to define at least one **source** XML tag with **Default** value (or without the **run-type** attribute). In addition one can also declare specific reference files for proton-proton, heavy ion and Cosmic run types.

Note that it's also possible to define multiple reference files for the same run type, in which case the files will be searched for a given reference object consecutively and the first one that is found will be used.

Here is a possible example of a run type dependent reference configuration:

```
<general>
  <variable name="REF_PATH" value="/atlas/moncfg/"/>
</general>
<reference>
  <source type="file:" value="${REF_PATH}/default_references.root"/>
  <source type="file:" run-type="HI" value="${REF_PATH}/hi_references.root"/>
  <source type="file:" run-type="PP" value="${REF_PATH}/pp_references.root"/>
  <source type="file:" run-type="Cosmic" value="${REF_PATH}/cosmic_references.root"/>
</reference>
```


