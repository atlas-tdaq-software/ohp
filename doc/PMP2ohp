Configuration file notes for the Online Histogram 
Presenter (ohp-01-00-00)

This file describes the configuration file format 
changes from the PMPpresenter to the new ohp.

For a complete configuration file example in the new
format, please refer to the file "example.conf" in the
"share" directory.

********************************************************
********************************************************
**                      Index                         **
********************************************************
********************************************************

- Unchanged blocks
- No more supported blocks
- New blocks
- Translating a configuration file

********************************************************
********************************************************
**                 Unchanged blocks                   **
********************************************************
********************************************************

The following blocks are unchanged and have the same
semantic as the ones for the PMPpresenter:

- PARTITION
- SERVERS
- DISPLAY

********************************************************
********************************************************
**            No more supported blocks                **
********************************************************
********************************************************

The following blocks are obsoleted:

- DRAWOPTIONS : It is now possible to set up several
                draw options per histogram, using the
                OPTIONS/<histogramName> blocks;

- HISTOGRAMS  : The syntax for the subscriptions has
                been changed. Use the SUBSCRIPTIONS
                block instead of this;

- PROVIDERS   : Same reason as above. HISTOGRAMS and
                PROVIDERS blocks had been unified
                using the SUBSCRIPTIONS block.

********************************************************
********************************************************
**                     New blocks                     **
********************************************************
********************************************************

- SUBSCRIPTIONS           : Unifies the semantics of the
                            HISTOGRAMS and PROVIDERS
                            blocks;

- OPTIONS/<histogramName> : Allows to specify several
                            drawing options for a
                            particular histogram;

- COMMANDS                : Allows to define custom
                            commands.

For a complete description of the semantic of every
block, please refer to the file "example.conf".

********************************************************
********************************************************
**          Translating a configuration file          **
********************************************************
********************************************************

To convert a configuration file valid for the 
PMPpresenter into one valid for the ohp, please follow
these steps:

1) Keep untouched the blocks PARTITION, SERVERS and
   DISPLAY;

2) Write a SUBSCRIPTIONS block starting from the
   informations present into the HISTOGRAMS and
   PROVIDERS blocks;

3) Add one or more OPTIONS/<histogramName> block(s)
   containing the desired drawing options, starting
   from the informations present into the old 
   DRAWOPTIONS block;

4) (optional): Erase the DRAWOPTIONS, HISTOGRAMS and
   PROVIDERS blocks. This step is not mandatory: if
   you leave these blocks, these will be simply
   ignored.
