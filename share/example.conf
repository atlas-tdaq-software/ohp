// This is an example of configuration file for the ohp.
//
// A valid configuration file is divided into blocks; every
// block represents a specific ohp beahviour to be configured.
//
// Every block has the following syntax:
//
// ->
// <block name>
// <option 1>
//   ...
// <option n>
// <-
//
// Where:
// 
// "->"            identifies the beginning of a block;
// 
// "<block name>"  is the name of the caratheristic to be configured.
//                 This name must be in upper case letters, unique 
//                 into the block and also into the whole file;
//
// "<option i>"    is a string setting a particular, valid, option
//                 belonging to a certain block;
//
// "<-"            identifies the end of a block.
//
// Double slashes "//" can be used for comments.
//
// To be valid, a configuration file must have a certain set of mandatory
// blocks, but is it also possible to specify some optional blocks.
// Some blocks must be written only once, but some others can be repeated.
// If a mandatory block misses, the ohp behaviour will be unpredictable.
//
// Here follows an example of a possible set of configuration blocks.

// The PARTITION block (mandatory, single) is used to specify the name of the
// used ipc partition.
// The partition name must be single; any other name eventually present
// into this block after the first one will be ignored.
->
PARTITION
be_test
<-

// The SERVERS block (mandatory, single) is used to specify the name of the
// histogram repository server (OHS). This name must be single; any other
// name eventually present into this block after the first one will
// be ignored. The reason of the plural form of this block name
// is to keep the compatibility of the configuration files between
// the ohp and its previous version (PMPpresenter).
->
SERVERS
Histogramming
<-

// The SUBSCRIPTIONS block (mandatory, single) is used to describe the list 
// of subscriptions to a particular provider and/or to a particular
// histogram set. Every subscription may contain regular expressions, in order
// to describe the set of histograms to be received. In this example,
// T.*/.* is a subscription to every provider with name beginning wihth "T" 
// and every histogram, while Provider/SomeFolder1/Histo1 is a 
// subscription to a single histogram
// published by the TileProv provider.
// ohp will not check for inclusion of subscriptions in others. This mean that:
//  .*/.*
//  Prov1/.*
// will cause ohp to receive twice notifications from Prov1 since .*/.* already
// includes Prov1/.*
->
SUBSCRIPTIONS
T.*/.*
Provider/SomeFolder/Histo1
<-

// The DISPLAY block (mandatory, single) is used to define the list of tabs, 
// for the shifter mode, and, for every tab, the list of its content.
// For each tab window, it is possible to specify its label
// (the text that will be shown as tab's label into the GUI)
// and the list of histograms to be shown inside the tab.
// If this block is empty, the ohp will be able to work only
// in browser mode. If you are not interested in tabs leave DISPLAY 
// block empty.
// 
// The syntax is:
// 
// <labelName>|<histogramName1>,<histogramName2>,...,<histogramNameN>
//
// Note that:
//
// 1) <labelName> must be separated from histogram names using a "|" character;
// 2) every <histogramName> must be separated from the other using a "," character;
// 3) every line must NOT contain empty spaces;
// 4) it is NOT possible to define a tab with no histograms; in other words,
//    every tab must contain at least one histogram;
// 5) it is NOT possible to show more than 64 histograms per tab;
// 6) the histogram names must NOT contain wild characters;
// 7) there must NOT be a semicolon after the last histogram name in the list;
// 8) every <histogramName> must be a complete name (provider/histogram).
//
// If these rules are not satisfied, the ohp's behaviour will be umpredictable.
->
DISPLAY
TileProv|TileProv/Tile/Drawer1/h1,TileProv/Tile/Drawer1/h2
Tile|CommonProv/Beam/h,TileAuxProv/Laser/h,TileAuxProv/MuWall/h,TileAuxProv/Phantom/h,TileAuxProv/Scin/SC1,TileAuxProv/Scin/SC2,TileAuxProv/Scin/SumK
<-

// The OPTIONS/<histogramName> blocks (optional, multiple) are used to 
// define a certain set of draw options for a particular histogram belonging 
// to a configured tab window.
// Options can be set only for histograms that were specified in at least one
// tab. If a histogram appears in more than one tab the draw options will
// be applied to all of them.
// This block doesn't affect browser mode.
// Every draw option is a pair (<key>,<value>). Every <key> must be an
// uppercase word; every <value> has a specific format, depending from
// <key>.
// For every option, the syntax is:
//
// <key> <value> [single space between key and value]
//
// Here follows the list of the supported keys:
// 
// REFERENCE:  allows the user to specify a particular reference
//             histogram for <histogramName>. <value> must be a
//             complete histogram name, WITHOUT wildcards.
//	       Histograms have always a reference histogram: the 
// 	       reference histogram has the same name as the received one, but
//	       it is searched in different providers or in a file 
//             (see later: REFERNCE block);
// 
// REFONTOP:   if the <value> for this option is "true" or "TRUE",
//             the reference histogram for <histogramName>, if 
//             present, will be drawn on the top of the canvas
//             in red color;
//
// STYLE:      it allows to specify a particular drawing style for
//             <histogramName>. The syntax for the <value> for
//             this draw option is exactly the name of the method
//             that would be called from a ROOT session using TStyle class.
//             If the STYLE syntax is wrong, the statement will
//             have no effect. The STYLE key can be repeated as many
//	       times as needed; 
//
// DRAWOPT:    it allows to specify a particular ROOT's drawing option,
//             for example LEGO (the valid options are the one that 
//             can be used with method TH1*::Draw(Option_t* opt) ). 
//             If the specified option is not valid,
//             this setting will have no effect;
//
// LINE:       allows to draw arbitrary lines over <histogramName>'s 
//             canvas. The syntax is: (x1,y1,x2,y2,Color,Style,Width),
//             the meaning of these parameters is explained in the 
//             ROOT Class Reference Manual for the TLine and TAttrLine 
//             objects.
// 
// REFDRAWOPT: allows to specify a ROOT's drawing options for the reference
//             histogram specified in the REFERENCE key (if specified).
//             Syntax and behaviour are the same as the DRAWOPT key.
//
// REFNORM:    if the <value> for this option is "true" or "TRUE",
//             the reference histogram for <histogramName>, if present, 
//             will be drawn normalized to received histogram.
//
// REFERENCE, REFONTOP, DRAWOPT, REFDRAWOPT and REFNORM must be present 
// only once; if these are repeated, only the last key will be considered.
// LINE and STYLE can be repeated.
//
// In the example below the Histogram Tile/Drawer1/h1 published by TileProv has
// the following options:
// the reference histogram name is:TileProv/Tile/Drawer1/h1Ref and it is drawn
// superimposed, the canvas has a grid on X axis, the axis X is logarithmic.
// The histogram is drawn with LEGO style and two lines are drawn in the same
// window. 
->
OPTIONS/TileProv/Tile/Drawer1/h1
REFERENCE /TileProv/Tile/Drawer1/h1Ref
REFONTOP true
STYLE SetOptLogx(1)
STYLE SetPadGridX(true)
DRAWOPT LEGO
LINE 0.2,1,0.5,3,kRed,3,4
LINE 0.2,1,0.5,3,kBlue,3,4
<-
->
OPTIONS/TileProv/Tile/Drawer1/h2
REFERENCE /TileProv/Tile/Drawer1/h2Ref
DRAWOPT LEGO
<-

// The REFERENCE block (optional, single) is used to specify the sources
// for the reference histograms. It is possible to specify multiple sources;
// the two possible sources are root files or providers
// publishing histograms in OHS. 
// 
// The syntax is:
//
// <key><value> [no spaces needed!]
//
// where <key> can be the string "prov://" (allowing to specify a subscription 
// to a provider containing reference histograms) or "file://" (allowing to 
// specify the name of a root file).
// The order of the specified sources is not relevant.
// If a wrong provider or a not existing file name is specified this will
// have no effect on ohp.
->
REFERENCE
file://ref.root
prov://RefProv/.*
file://ref2.root
file://ref3.root
prov://RefProv1/Ref/.*
<-

// The COMMAND block is used to specify the user defined commands.
// It is possible to specify a user-defined command name and the
// list of the parameters names.
// The syntax is:
//
// <commandName>|<parName1>|<parName2>|<parName3>|<parName4>|<parName5>|<parName6>|<parName7>|<parName8>|<parName9>
//
// Up to a maximum of 9 parameters are accepted.
// Please note that the producer must be able to process the defined command.
// The correct syntax is, therefore, producer dependant.
// This block has the only effect to add an entry in the send commands panel.
->
COMMANDS
custom1|p1|p2|p3|p4|p5|p6|p7|p8|p9
<-
