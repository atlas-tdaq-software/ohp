/*  FILE:	main.cpp
    AUTHOR:	Danilo Cimino [cimino@cli.di.unipi.it]
    MODIFIED:	Serguei Kolos [Serguei.Kolos@cern.ch]
*/
#include <string>

#include <QApplication>
#include <QSplashScreen>

#include <TApplication.h>
#include <TROOT.h>
#include <TSystem.h>

#include <HistogramStyles/HistogramStyles.h>
#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <ohp/PluginBase.h>
#include <ohp/PluginManager.h>
#include <ohp/CoreInterface.h>


class Timer : public QObject
{
  public:
    Timer() : m_id(startTimer(10))
    { }

    ~Timer() {
        killTimer(m_id);
    }

  private:
    void timerEvent(QTimerEvent * ) {
        gSystem->ProcessEvents();
    }
    const int m_id;
};

int startOHP(int argc, char ** argv, const std::string& partition,
        const std::string& config_file)
{
    TApplication tapp("Online Histogram Presenter", &argc, argv);
    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(true);

    HistogramStyles::rootUtils = new HistogramStyles();
    gROOT->SetStyle("ATLAS");

    QPixmap pixmap(":/logo.png");
    QSplashScreen splash(pixmap, Qt::SplashScreen | Qt::WindowStaysOnTopHint);
    splash.show();
    app.processEvents(QEventLoop::AllEvents);

    try {
	ohp::CoreInterface& ci =
	        ohp::CoreInterface::getInstance(partition, config_file);
	boost::shared_ptr<ohp::PluginBase> main = ci.getMainPlugin();
        main->getWidget()->show();
        splash.finish(main->getWidget());
    }
    catch (ers::Issue & ex) {
	ers::fatal(ex);
	return 1;
    }

    Timer timer; // required to animate ROOT menus
    return app.exec();
}


int main( int argc, char ** argv )
{
    try {
	IPCCore::init(argc,argv);
    }
    catch(daq::ipc::Exception & ex ) {
	ers::fatal(ex);
	return 1;
    }

    CmdArgStr partition_name('p', "partition", "part_name",
            "Partition name.");
    CmdArgStr configuration_file('c', "config", "conf_file",
            "Configuration file name", CmdArg::isREQ);
    CmdLine cmd( *argv, &partition_name, &configuration_file, 0);
    CmdArgvIter arg_iter( --argc, (const char* const *)++argv );
    cmd.description( "Online Histogram Presenter" );

    partition_name = "";
    cmd.parse( arg_iter );

    std::string config_file((const char*)configuration_file);
    std::string::size_type pos = config_file.rfind("/");

    if (pos != std::string::npos) {
	std::string config_file_origin = config_file.substr(0, pos);
	const char * env = getenv("TDAQ_OHP_PATH");
	std::string new_search_path = config_file_origin + ":" + std::string(env ? env : "");
	setenv("TDAQ_OHP_PATH", new_search_path.c_str(), true);
    }

    return startOHP(argc, argv, (const char*)partition_name, (const char*)configuration_file);
}
