/*
  FILE :	Configuration.h
  AUTHOR :	Danilo Cimino [cimino@cli.di.unipi.it]
		Paolo Adragna [p.adragna@qmul.ac.uk]
		Andrea Dotti  [andrea.dotti@pi.infn.it]
  MODIFIED:	Serguei Kolos
*/

#ifndef OHP_CONFIGURATION_H
#define OHP_CONFIGURATION_H

#include <list>
#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/xpressive/xpressive.hpp>

#include <ers/ers.h>

#include <ohp/mxml/mxml.h>
#include <ohp/Section.h>
#include <ohp/PluginConfig.h>

#include <dqm_config/RunType.h>

ERS_DECLARE_ISSUE ( ohp,
                    BadConfiguration,
                    error,
                    ((std::string)error )
                  )

namespace ohp
{  
  /// A Configuration object represents a certain configuration for the ohp.
  /**
   * This object can read XML configuration files.
   * It manages a set of Section objects, which
   * provide an internal representation of the OHP
   * configuration files.
   */
  class Configuration
  {
  public:
    /**
     * @param file_name The configuration file name
     */
    Configuration(const std::string& partition_name, const std::string& file_name);
    
    /** 
     * @return The configuration block with the @param histogram, if it exists, or NULL otherwise
     */
    const Section* getOptions(const std::string& tabname, const std::string& histogram) const;
    
    /** 
     * @return The configuration block with the @param histogram, if it exists, or NULL otherwise
     */
    const Section* getGlobalOptions(const std::string& histogram) const;
    
    /**
     * @return all global options which are matching the @param histogram
     */
    std::vector<const ohp::Section*> getMatchedOptions(const std::string& histogram) const;
    
    /**
     * Dumps parsed configuration to the output stream
     */
    void dump(std::ostream & out) const;
    
    const std::string & getPartition() const { return m_partition; }
    
    const std::vector<std::string> & getServers() const { return m_servers; }
    
    const std::vector<std::pair<dqm_config::RunType, std::string>> & getFiles() const { return m_files; }
    
    const std::vector<PluginConfig> & getPlugins() const { return m_plugins; }
            
  private:    
    void readFromXML(const std::string & partition_name, const std::string & file_name);
    void readPluginOptions(PMXML_NODE node, PluginInfo& parent);
    void readHistogramOptions(PMXML_NODE node, const std::string & options_name, ohp::Sections& sections);

    void addServer(const std::string& server);
    void addFile(const std::string& file, dqm_config::RunType rt);
    
    std::string replaceOne(const boost::xpressive::smatch & match) const;
    std::string replaceAll(const std::string& token) const;
    void replaceVariables();

  private:
    struct Variable
    {
    	Variable(const std::string& name, const std::string& value)
          : m_name(name),
            m_value(value)
        { ; }
        
        const std::string m_name;
        const std::string m_value;
    };

    typedef boost::multi_index_container<
	Variable,
	indexed_by<
	    hashed_unique <
		member<Variable,const std::string,&Variable::m_name>
	    >
	>
    > Variables; 
    
  private:      
    Sections m_options;
    Sections m_global_options;
    std::string m_partition;
    std::vector<std::string> m_servers;
    std::vector<std::pair<dqm_config::RunType, std::string>> m_files;
    std::vector<PluginConfig> m_plugins;
    Variables m_vars;
  };
}

#endif
