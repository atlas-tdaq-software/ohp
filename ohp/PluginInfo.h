/*
  FILE:         PluginInfo.h
  AUTHOR:       Paolo Adragna [p.adragna@qmul.ac.uk]
  MODIFIED:	Serguei Kolos
*/

#ifndef OHP_PLUGININFO_H
#define OHP_PLUGININFO_H

#include <vector>
#include <string>
#include <algorithm>
#include <functional>

#include <boost/lexical_cast.hpp>

#include <ers/ers.h>

#include <ohp/Section.h>

ERS_DECLARE_ISSUE ( ohp,
                    PluginInfoNotFound,
                    "Information about '" << name << "' plugin is not found",
                    ((std::string)name )
                  )

namespace ohp 
{
    /// Class representing the informations associated to the plugins
    class PluginInfo : public Section
    {
      public:
	PluginInfo(const std::string& name)
          : Section(name)
        { ; }
        
	PluginInfo(const Section& s)
          : Section(s)
        { ; }
        
	void addSection(const ohp::Section & section)
        { Section::m_subsections.get().insert(section); }
        
        PluginInfo getInfo(const std::string& name) const;
        
        /** Get a parameter
	 *  Get a parameter from configuration. 
	 *  The parameter is stored in value, returns false on failure, true on success
	 */
	bool getParameter(const std::string& name, bool& value) const;

	/** Get a vector of parameters
	 *  Get value for a multi-value parameter from configuration. 
	 *  The parameter is stored in value, returns false on failure, true on success
	 */
	bool getParameters(const std::string& name, std::vector<bool>& values) const;

	/** Get a vector of vectors
	 *  Get all values of a multi-value parameter from configuration. 
	 *  The parameter is stored in value, returns false on failure, true on success
	 */
	bool getParameters(const std::string& name, std::vector<std::vector<std::string> >& values) const;
	
        /** Get a parameter
	 *  Get a parameter from configuration. 
	 *  The parameter is stored in value, returns false on failure, true on success
	 */
	template <class T>
        bool getParameter(const std::string& name, T& value) const;

	/** Get a vector of parameters
	 *  Get value for a multi-value parameter from configuration. 
	 *  The parameter is stored in value, returns false on failure, true on success
	 */
	template <class T>
	bool getParameters(const std::string& name, std::vector<T>& values) const;

    };
    
    template <class T>
    bool
    ohp::PluginInfo::getParameter(const std::string& name, T& value) const
    {
	Options::const_iterator it = m_options.find(name);
	if (it == m_options.end())
	    return false;

	value = boost::lexical_cast<T>(it->m_values[0]);
	return true;
    }

    template <class T>
    bool
    ohp::PluginInfo::getParameters(const std::string& name, std::vector<T>& values) const
    {
	Options::const_iterator it = m_options.find(name);
	if (it == m_options.end())
	    return false;

	values.resize(it->m_values.size());
        std::transform(it->m_values.begin(), it->m_values.end(), 
        		values.begin(), 
                        [](const std::string & s){return boost::lexical_cast<T>(s);});
	return true;
    }
}

#endif
