/*
  FILE:		FileHistogramManager.h
  AUTHOR:	Serguei Kolos  																				 ARC Cache support added
*/

#ifndef OHP_FILE_HISTOGRAM_MANAGER_H
#define OHP_FILE_HISTOGRAM_MANAGER_H

#include <boost/regex.hpp>

#include <ohp/FileManager.h>
#include <ohp/HistogramManager.h>

namespace ohp
{      
    class FileHistogramManager : public HistogramManager
    {
    public:
	/// Creates new FileHistogramManager
	FileHistogramManager(const std::string& file_name);
	
        /// Adds regex subscription
	void attach(const std::string& regular_expression, ohp::PluginBase& plugin);
    
	/// Removes regex subscription
	void detach(const std::string& regular_expression, ohp::PluginBase& plugin);
        	
	TObject* readObject(const std::string& name, Annotations& annotations, Time& universal_time);

        void getHistogramsList(	const std::string & server, 
				const std::string & providers_regex, 
				const std::string & histograms_regex,
                                std::vector<HistogramNameTokens>& histograms) const;

        uint32_t getRunNumber() const {
            return m_run_number;
        }

        uint32_t getLumiBlock() const {
            return m_lumi_block;
        }

    private:
	void buildHistogramsList(  TDirectory* provider,
				    const std::string & server_name,
				    const std::string & provider_name,
				    const std::string & path_name,
				    const boost::regex & histograms_regex,
				    std::vector<HistogramNameTokens>& histograms) const;
    private:
    	Time				m_time;
	boost::shared_ptr<TFile>	m_file;
	uint32_t                        m_run_number = -1;
	uint32_t                        m_lumi_block = -1;
    };
}

#endif
