/*
  FILE:         SubscriptionFilter.h
  AUTHOR:	Serguei Kolos
*/

#ifndef OHP_SUBSCRIPTION_FILTER_H
#define OHP_SUBSCRIPTION_FILTER_H

#include <shared_mutex>
#include <string>
#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>

using namespace boost::multi_index;

namespace ohp
{
  class PluginBase;
  
  /// Manages the corrispondences among patterns and plugins
  class SubscriptionFilter
  {
  public:      
    /// Changes the filtering pattern
    void subscribe(const std::vector<std::string>& histogram_names, ohp::PluginBase& plugin);
        
    unsigned int histogramUpdated(const std::string & name);
    
  private:    
    void notifyPlugins(const std::string & histogram);

  public:
  
    struct Object
    {
    	const std::string	m_name;
    	const std::string	m_plugin_name;
        ohp::PluginBase&	m_plugin;
        
        Object(const std::string & name, ohp::PluginBase& plugin);
    };
    
    typedef boost::multi_index_container<
	Object,
	indexed_by<
	    hashed_non_unique <
		BOOST_MULTI_INDEX_MEMBER(Object,const std::string,m_name)
	    >,
	    hashed_non_unique <
		BOOST_MULTI_INDEX_MEMBER(Object,const std::string,m_plugin_name)
	    >
	>
    > Objects;
    
    typedef Objects::nth_index<1>::type Plugins;
    
  private:
    std::shared_mutex m_mutex;
    
    Objects	m_objects;
  };
}

#endif
