/*
  FILE :	Options.h
  AUTHOR:	Serguei Kolos
*/

#ifndef OHP_OPTIONS_H
#define OHP_OPTIONS_H

#include <algorithm>
#include <string>
#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/bind/bind.hpp>

using namespace boost::multi_index;
using namespace boost::placeholders;

namespace ohp
{
    struct Option
    {
	Option(const std::string& key)
	  : m_key(key)
	{ ; }
      
	Option(const std::string& k, const std::string& v)
	  : m_key(k)
	{ m_values.push_back(boost::trim_copy(v)); }
      
	Option(const std::string& k, const std::vector<std::string>& v)
	  : m_key(k),
	    m_values(v)
	{ std::for_each(m_values.begin(), m_values.end(),
        		boost::bind(&boost::trim<std::string>,_1, std::locale())); }
        
        void replaceVariables(boost::function<std::string (const std::string&)> op) const
        { std::transform(m_values.begin(), m_values.end(), 
        		 m_values.begin(), op); }
        
	const std::string	 	 m_key;
	mutable std::vector<std::string> m_values;
    };
  
    typedef boost::multi_index_container<
	Option,
	indexed_by<
	    hashed_non_unique <
		BOOST_MULTI_INDEX_MEMBER(Option,const std::string,m_key)
	    >
	>
    > Options;
}

#endif
