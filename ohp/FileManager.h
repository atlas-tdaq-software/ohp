/*
  FILE:		FileManager.h
  AUTHOR:	Danilo Cimino [cimino@cli.di.unipi.it], Nov 2005
  MODIFIED:	Serguei Kolos
*/

#ifndef OHP_FILE_MANAGER_H
#define OHP_FILE_MANAGER_H

#include <vector>
#include <string>

#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>

#include <dqm_config/RunType.h>

class TFile;
class TObject;

namespace ohp {
    
  /// Provides a function to get histograms from TFiles
  class FileManager {

  public:
    
    /**
     * Default constructor
     */
    FileManager() { ; }
    
    /**
     * @param f A list of TFile names
     */
    FileManager(const std::vector<std::pair<dqm_config::RunType, std::string>>& v);
      
    /// Gets a histogram from one of the managed TFiles
    /**
     * @param h The histogram name
     *
     * @return A pointer to the histogram h, or NULL if it doesn't exist
     */
    TObject* readHistogram(const std::string& h, dqm_config::RunType rt);
    
    void addFile(const std::string& file_name, dqm_config::RunType rt);

  private:
    boost::mutex m_mutex;
    std::vector<std::pair<dqm_config::RunType, boost::shared_ptr<TFile>>> m_files;
  };
}

#endif //TFILEMANAGER_H
