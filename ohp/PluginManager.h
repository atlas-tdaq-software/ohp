/*
  FILE:         PluginManager.h
  AUTHOR:       Paolo Adragna [p.adragna@qmul.ac.uk]
  MODIFIED:	Serguei Kolos
*/

#ifndef OHP_PLUGINMANAGER_H
#define OHP_PLUGINMANAGER_H

#include <map>
#include <vector>
#include <string>
#include <list>

#include <boost/shared_ptr.hpp>

#include <ers/ers.h>

#include <ohp/SharedLibrary.h>
#include <ohp/PluginConfig.h>

ERS_DECLARE_ISSUE ( ohp,
                    PluginNotFound,
                    "'" << name << "' plugin is not found",
                    ((std::string)name )
                  )

ERS_DECLARE_ISSUE ( ohp,
                    PluginCreatorNotFound,
                    "Creator for the '" << name << "' plugin is not found",
                    ((std::string)name )
                  )
namespace ohp 
{
    class PluginBase;
    typedef std::map<std::string,boost::shared_ptr<ohp::PluginBase> >	Plugins;
  
  /// Manages the plugins loading, configuration and registration
    class PluginManager
    {
      public:    
	void loadPlugins(const std::vector<PluginConfig>& plugins);

	boost::shared_ptr<ohp::PluginBase> getPlugin(const std::string& name) const;

	boost::shared_ptr<ohp::PluginBase> getMainPlugin() const;

	const Plugins& getPlugins() const { return m_plugins; }

	void registerPlugin(const std::string& key,
		    boost::function<PluginBase* (const std::string& )> creator);

      private:
	boost::shared_ptr<ohp::PluginBase> createPlugin(const PluginConfig& info);

      private:
	typedef std::map<std::string,boost::shared_ptr<ohp::SharedLibrary> >		    Libraries;
	typedef std::map<std::string,boost::function<PluginBase* (const std::string&)> >    Creators;

	Libraries			    m_libraries;
	Creators			    m_creators;
	Plugins				    m_plugins;
	boost::shared_ptr<ohp::PluginBase>  m_main_plugin;
    };
}

#endif // PLUGINMANAGER_H
