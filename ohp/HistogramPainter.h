/*
 FILE:		HistogramPainter.h
 AUTHOR:	Danilo Cimino [cimino@cli.di.unipi.it]
 MODIFIED:	Serguei Kolos
 DESCRIPTION:	A helper class for drawing histograms with associated options/metadata.
*/

#ifndef OHP_HISTOGRAM_DRAW_H
#define OHP_HISTOGRAM_DRAW_H

#include <string>

#include <TClass.h>
#include <TPad.h>
#include <TLine.h>

#include <ohp/CoreInterface.h>

namespace ohp
{
  namespace histogram
  {
    /// Draw histogram in the specified TPad object using confguration options
    bool drawWithConfig(const std::string& tab, 
    			const std::string& histogram_name, 
                        TPad* pad,
                        const std::string& options = std::string());

    /// Draw histogram in the specified TPad object without applying confguration options
    void draw(	const std::string& histogram_name, 
    		TPad* pad, 
                const std::string& options = std::string());
  }
}

#endif
