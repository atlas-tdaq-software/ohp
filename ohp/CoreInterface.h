/*
 FILE:		CoreInterface.h
 AUTHORS:	Paolo Adragna [p.adragna@qmul.ac.uk]
 MODIFIED:	Serguei Kolos
 */

#ifndef OHP_CORE_INTERFACE_H
#define OHP_CORE_INTERFACE_H

#include <string>
#include <vector>

#include <TObject.h>

#include <ers/ers.h>
#include <oh/OHCommandSender.h>

#include <ohp/Event.h>
#include <ohp/HistogramProperties.h>
#include <ohp/InfrastructureChecker.h>

namespace ohp 
{
    class Configuration;
    class HistogramManager;
    class PluginBase;
    class PluginManager;
    class FileManager;
    
    typedef std::map<std::string,boost::shared_ptr<ohp::PluginBase> >		Plugins;
    typedef std::vector<std::pair<std::string,boost::shared_ptr<TObject> > > 	Histograms;
    typedef std::vector<std::pair<std::string,std::string> >			Annotations;
    typedef boost::posix_time::ptime						Time;

    /**
     * A type identifying an histogram complete name containing: server name, provider name and histogram name
     */
    struct HistogramNameTokens {
	HistogramNameTokens() { ; }
        
	HistogramNameTokens(
        	const std::string & s, 
                const std::string & p, 
                const std::string & h,
                const std::string & o = "")
          : serverName(s),
            providerName(p),
            histogramName(h),
            oldHistogramName(o.empty() ? h : o)
        { ; }
        
        std::string serverName; 	/** server name */
	std::string providerName;	/** provider name */
	std::string histogramName;  	/** histogram name */
	std::string oldHistogramName;  	/** histogram name which has no leading slash */
    };

    class CoreInterface
    {
    public:
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdangling-reference"
        static CoreInterface & getInstance(const std::string & partition_name="",
        				   const std::string& configuration_file="");
#pragma GCC diagnostic pop
        
        static TObject* cloneObject(TObject* object);

	static bool tokenize(const std::string& fullHistoName, HistogramNameTokens& tokens);

    public:
	void infrastructureUp();
	
        void infrastructureDown();
        
        bool isOnline() const;
        
	void serverUp(const std::string& name);
	
        void serverDown(const std::string& name);
        
        void registerPlugin(const std::string& key,
		    boost::function<PluginBase* (const std::string& )> creator);

        void loadPlugins();
        
        const Plugins& getPlugins() const;
        
	boost::shared_ptr<ohp::PluginBase> getMainPlugin() const;
        
	void getHistogramsList(	const std::string & server, 
				const std::string & providers_regex, 
				const std::string & histograms_regex,
				std::vector<HistogramNameTokens>& histograms) const;
                                
        void getHistograms(Histograms& histograms) const;
        
        void setHistogramSource(boost::shared_ptr<HistogramManager> manager);

        /// Changes the filtering pattern: plugins version.
	void subscribe(const std::vector<std::string>& histogram_names, PluginBase& plugin);

        /// Subscribe to the given regular expression.
	void subscribe(const std::string& regex, PluginBase& plugin);

        /// Cancel the given subscription
	void unsubscribe(const std::string& regex, PluginBase& plugin);

	/// Gets the number of the notifications received from the OHS
	unsigned int getInNotifications() const;

	/// Gets the number of the notifications sent to the GUI
	unsigned int getOutNotifications() const;

	/// Sends command to the OHS
	bool sendCommand(const std::string& histoCompleteName, const std::string& cmd, bool toProvider=false);

	/// Gets a histogram
	TObject* getHistogram(const std::string& histogram);

	/// Gets a histogram
	TObject* getHistogram(const std::string& histogram, Annotations& ann, Time& time);

	/// Gets a histogram
	TObject* getReferenceHistogram(const std::string& histogram, bool search_in_oh);

	/// Returns object which contains in-memory representation of the actual configuration
	const Configuration& getConfiguration() const;

	/// Returns CoreInterface associated servers name
	const std::vector<std::string>& getServers() const;

	/// Sets the properties of the histogram histogram
	void setProperty(const std::string& histogram, const std::string& key, const std::string& value);

	/// Sets the properties of the histogram histogram
	void setProperty(const std::string& histogram);

	/// Gets the properties associated to a histogram
	const Histogram::Properties* getHistogramProperties(const std::string& histogram) const;

	/// Returns the associated partition object
	IPCPartition getPartition() const;

	/// Used to send an event to every registered plugin(s)
	void broadcastEvent(Event::Type e, const std::string& data = std::string());

	void manageRootFile(const std::string& file_name,
	        dqm_config::RunType rt = dqm_config::RunType::Default);

	const std::string & getHistogramDocumentation(const std::string& histogram) const;
	
        void pause();
        
        void resume();

        uint32_t getRunNumber() const;

        uint32_t getLumiBlock() const;

    private:
	CoreInterface(const std::string & partition_name, const std::string& configuration_file);
	
    private:        
	boost::shared_ptr<Configuration>	 m_configuration;
	boost::shared_ptr<HistogramManager>	 m_histogram_manager;
	boost::shared_ptr<FileManager>		 m_file_manager;
        boost::shared_ptr<PluginManager>	 m_plugin_manager;
	boost::shared_ptr<HistogramProperties>	 m_histogram_properties;
	boost::shared_ptr<InfrastructureChecker> m_infrastructure_checker;
	OHCommandSender				 m_command_sender;
    };
}

#endif
