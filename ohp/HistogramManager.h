/*
  FILE:		HistogramManager.h
  AUTHOR:	Serguei Kolos  																				 ARC Cache support added
*/

#ifndef OHP_HISTOGRAM_MANAGER_H
#define OHP_HISTOGRAM_MANAGER_H

#include <ohp/Cache.h>
#include <ohp/SubscriptionFilter.h>
#include <ohp/CoreInterface.h>

using namespace boost::multi_index;

namespace ohp
{      
    class HistogramManager
    {
    public:	
	HistogramManager();
        
        virtual ~HistogramManager() { ; }
        
	virtual TObject* readObject(const std::string& name, Annotations& annotations, Time& universal_time) = 0;
        
        virtual void getHistogramsList(	const std::string & server, 
					const std::string & providers_regex, 
					const std::string & histograms_regex,
                                        std::vector<HistogramNameTokens>& histograms) const = 0;
                                        
	virtual void attach(const std::string& , ohp::PluginBase& ) { ; }
    
	virtual void detach(const std::string& , ohp::PluginBase& ) { ; }
        
	virtual void infrastructureUp() { ; }
	
        virtual void infrastructureDown() { ; }
        
        virtual bool isOnline() const { return true; }
                
	virtual TObject* getHistogram(const std::string& name, Annotations& annotations, Time& universal_time);
        
        virtual void getHistograms(Histograms& histograms) const { m_cache.getHistograms(histograms); }
	
	/// Gets the number of the histogram updates notifications received
	virtual unsigned int getInNotifications() const { return m_in_cnt; }

	/// Gets the number of the notifications sent to the GUI
	virtual unsigned int getOutNotifications() const { return m_out_cnt; }
        
        virtual void pause() { m_paused = true; }

	virtual void resume() { m_paused = false; }

        virtual void histogramUpdated(const std::string& name);
        
        virtual void subscribe(const std::vector<std::string>& histogram_names, ohp::PluginBase& plugin);

        virtual uint32_t getRunNumber() const = 0;

        virtual uint32_t getLumiBlock() const = 0;

    protected:
        void reset();
        
    protected:
	SubscriptionFilter	m_filter;
	Cache			m_cache;
	unsigned int		m_in_cnt;
	unsigned int		m_out_cnt;
	bool			m_paused;
    };
}

#endif
