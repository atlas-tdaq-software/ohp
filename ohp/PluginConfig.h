/*
  FILE:         PluginConfig.h
  AUTHOR:       Serguei Kolos
*/

#ifndef OHP_PLUGINCINFIG_H
#define OHP_PLUGINCINFIG_H

#include <list>
#include <vector>
#include <string>

#include <ers/ers.h>

#include <ohp/PluginInfo.h>

namespace ohp 
{
    /// Class representing the informations associated to the plugins
    class PluginConfig : public PluginInfo
    {
      public:
	PluginConfig(	
	    const std::string& name,
	    const std::string& type,
	    const std::string& library)
          : PluginInfo(name),
            m_type(type),
            m_library(library)
        { ; }
        
        const std::string& getType() const { return m_type; }

	const std::string& getLibrary() const { return m_library; }

        void replaceVariables(boost::function<std::string (const std::string&)> op) { 
	    PluginInfo::replaceVariables(op);
            m_type    = op(m_type); 
	    m_library = op(m_library); 
        }
        
      private:
	std::string m_type;
	std::string m_library;
    };
}

#endif
