/*
  FILE:		Cache.h
  AUTHOR:	Serguei Kolos  																			       ARC Cache support added
*/

#ifndef OHP_CACHE_H
#define OHP_CACHE_H

#include <string>
#include <vector>

#include <TObject.h>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/function.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace boost::multi_index;

namespace ohp 
{
    typedef std::vector<std::pair<std::string,std::string> > 				Annotations;
    typedef std::vector<std::pair<std::string,boost::shared_ptr<TObject> > > 		Histograms;
    typedef boost::posix_time::ptime							Time;
    typedef boost::function<TObject* (const std::string& , Annotations& , Time&)>	ObjectReader;

    class Cache
    {
      public:
      
        Cache(const ObjectReader& reader)
          : m_object_reader(reader)
        { ; }
        
	std::string objectChanged(const std::string& name);

	TObject* getObject(const std::string& name, Annotations& annotations, Time& universal_time);
        
        void getHistograms(Histograms& histograms) const;
        
        void clear();
        
      private:
	struct Element
	{
	    const std::string			    m_name;
	    mutable boost::shared_ptr<TObject>	    m_object;
	    mutable Annotations			    m_annotations;
	    mutable Time			    m_universal_time;
	    mutable bool			    m_up_to_date;

	    Element(const std::string & name)
	      : m_name(name),
		m_up_to_date(false)
	    { ; }

	    Element(const std::string & name, TObject * object, const Annotations& annotations)
	      : m_name(name),
		m_object(object),
		m_annotations(annotations),
		m_up_to_date(object)
	    { ; }

	    void invalidate() const { m_up_to_date = false; }
	};

	typedef boost::multi_index_container<
	    Element,
	    indexed_by<
		hashed_unique <
		    BOOST_MULTI_INDEX_MEMBER(Element,const std::string,m_name)
		>
	    >
	> Data;
      
      private:
	mutable boost::mutex	m_mutex;
        ObjectReader		m_object_reader;
	Data			m_data;
    };
}

#endif
