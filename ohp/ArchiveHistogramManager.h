/*
  FILE:		ArchiveHistogramManager.h
  AUTHOR:	Serguei Kolos  																				 ARC Cache support added
*/

#ifndef OHP_ARCHIVE_HISTOGRAM_MANAGER_H
#define OHP_ARCHIVE_HISTOGRAM_MANAGER_H

#include <vector>

#include <mda/mda.h>

#include <ohp/HistogramManager.h>

namespace ohp
{      
    class ArchiveHistogramManager : public HistogramManager
    {
    public:
	/// Creates new ArchiveHistogramManager
	ArchiveHistogramManager(int number_of_runs);
	
        /// Adds regex subscription
	void attach(const std::string& regular_expression, ohp::PluginBase& plugin);
    
	/// Removes regex subscription
	void detach(const std::string& regular_expression, ohp::PluginBase& plugin);
        
        bool isOnline() const { return false; }
        
        const std::vector<unsigned int>& getRunNumbers() const { return m_run_numbers; }
        
        const std::set<daq::mda::RunInfo>& getRunInfos() const { return m_runs; }
        
        void setRunNumber(unsigned int rn);
        	
	TObject* readObject(const std::string& name, Annotations& annotations, Time& universal_time);
        
        void getHistogramsList(	const std::string & server, 
				const std::string & providers_regex, 
				const std::string & histograms_regex,
                                std::vector<HistogramNameTokens>& histograms) const;

        uint32_t getRunNumber() const {
            return m_selected_run_number;
        }

        uint32_t getLumiBlock() const {
            return -1;
        }

    private:        
        class Files : public daq::mda::FileRead
        {
	    typedef std::map<std::string, boost::shared_ptr<TFile> > Cache;
            
          public:
            Files(boost::shared_ptr<daq::mda::DBRead> db) 
	      : m_db(db) 
	    { ; }
            
            TObject* readObject(unsigned int run_number, const std::string& name);
            
            void clear() { m_cache.clear(); }
            
          private:
	    boost::shared_ptr<daq::mda::DBRead>	m_db;
            Cache				m_cache;
        };
                
    private:
        boost::shared_ptr<daq::mda::DBRead>	m_db;
        Files					m_files;
        std::set<daq::mda::RunInfo>		m_runs;
    	std::vector<unsigned int>		m_run_numbers;
        unsigned int				m_selected_run_number;
        Time					m_selected_run_time;
    };
}

#endif
