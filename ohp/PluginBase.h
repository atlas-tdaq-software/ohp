/*
  FILE:         PluginBase.h
  AUTHOR:       Andrea Dotti [andrea.dotti@pi.infn.it]
  MODIFIED: 	Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Mid 2008) -- MDI support added
  MODIFIED:	Serguei Kolos
*/


#ifndef OHP_PLUGINBASE_H
#define OHP_PLUGINBASE_H

#include <string>

#include <boost/preprocessor/cat.hpp>
#include <boost/noncopyable.hpp>

#include <ohp/CoreInterface.h>
#include <ohp/PluginInfo.h>

#define OHP_REGISTER_PLUGIN( class, name ) \
namespace { \
    struct BOOST_PP_CAT( PluginRegistrator, __LINE__ ) { \
        static ohp::PluginBase * create(const std::string& name) \
        { return new class(name); }  \
        BOOST_PP_CAT( PluginRegistrator, __LINE__ ) ()\
        { ohp::CoreInterface::getInstance().registerPlugin( name, &create ); } \
    } BOOST_PP_CAT( registrator, __LINE__ ); \
}

class QEvent;
class QWidget;
class QObject;
class QSettings;

namespace ohp {

  enum PluginType{
	  BROWSER,
	  CANVAS,
          CUSTOM,
	  MAIN,
          STATUS,
          TABLE
  };

  /// The base class for an ohp's plugin
  class PluginBase : boost::noncopyable
  {
  public:
    virtual ~PluginBase();

    /// @return plugin instance name
    const std::string& getName() const { return m_name; };

    /// @return plugin instance name
    const std::string& getDoc() const { return m_doc; };

    /**
     * Get the plugin type
     * @returns the plugin type
     */
    PluginType getType() const { return m_type; };
        
    /** Get the QtGUI Interface
	@return pointer to the Qt Interface for the plugin.
	returns NULL if no GUI is defined
    */
    QWidget* getWidget() const { return m_widget; };

    void subscribe(const std::vector<std::string>& histogram_names = std::vector<std::string>());

    void postEvent(QEvent* event);
    
  protected:
    /** Constructor
	@param type plugin type
    **/
    PluginBase(const std::string& name, PluginType myType, QWidget* widget);

    const char* name()
    { return m_name.c_str(); }
    
    /** Called by concrete plugin to set the documentation */
    void setDoc(const std::string& doc);

    /** Called by concrete plugin to set the main widget */
    void setWidget(QWidget* widget);
        
 public:
    /** Called at configure */
    virtual void configure(const PluginInfo& ) { ; }

    /** Called before displaying for the first time */
    virtual void aboutToDisplay() { ; }
    
    /** Called when plugin has to refresh the content it displays */
    virtual void updateContent() { ; }
        
    /** Called when histogram input source is updated */
    virtual void histogramSourceChanged() { ; }
        
    /** Called before exiting to allow plugin to save its state */
    virtual void saveState(QSettings& ) { ; }
        
    /** Called before displaying plugin to allow plugin to restore its state */
    virtual void restoreState(const QSettings& ) { ; }
        
  private:
    std::string	m_name;
    PluginType	m_type;
    std::string	m_doc;
    QWidget*	m_widget;
  };
}

#endif
