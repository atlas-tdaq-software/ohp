/*
  FILE:        InfrastructureChecker.h
  AUTHOR:      Danilo Cimino [cimino@cli.di.unipi.it]
  DESCRIPTION: A Thread that periodically checks whether the IPCPartition 
               and the IS server are up and running.
  MODIFIED:    Roberto Agostino Vitillo [vitillo@cli.di.unipi.it] (Early 2008) -- Multiple server support added
*/

#ifndef INFRASTRUCTURECHECKER_H
#define INFRASTRUCTURECHECKER_H

#include <string>   
#include <vector>

#include <ipc/alarm.h>
#include <ipc/partition.h>
#include <dqm_config/AlgorithmConfigFactory.h>

namespace ohp {

  class CoreInterface;
  
  class InfrastructureChecker
  {
  public:
    /*
     * Set the list of connected servers
     * @param serversName list of connected servers
     */
    InfrastructureChecker(const IPCPartition & p, const std::vector<std::string>& servers);
    
    dqm_config::RunType getRunType() const {
        return m_config_factory.getRunType();
    }

  private:
    bool periodic_action();
        
  private:
    std::vector<std::string> m_servers; /** List of checked servers */
    std::vector<std::string> m_down_servers; /** List of checked servers */
    IPCPartition m_partition;
    bool m_up_sent;
    bool m_down_sent;
    dqm_config::AlgorithmConfigFactory m_config_factory;
    IPCAlarm m_alarm;
  };
}
#endif // INFRASTRUCTURECHECKER_H
