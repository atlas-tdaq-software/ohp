/*
  FILE:		OnlineHistogramManager.h
  AUTHOR:	Serguei Kolos  																				 ARC Cache support added
*/

#ifndef OHP_ONLINE_HISTOGRAM_MANAGER_H
#define OHP_ONLINE_HISTOGRAM_MANAGER_H

#include <atomic>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/thread/mutex.hpp>

#include <is/infowatcher.h>
#include <oh/OHSubscriber.h>
#include <oh/OHRootReceiver.h>
#include <TTCInfo/LumiBlock.h>
#include <rc/RunParams.h>

#include <ohp/HistogramManager.h>

using namespace boost::multi_index;

namespace ohp
{      
    class OnlineHistogramManager : public HistogramManager,
    				   public OHRootReceiver
    {
    public:
	/// Creates new OnlineHistogramManager
	OnlineHistogramManager(const IPCPartition& partition);
	
	/// Searches for the histogram
	TObject* getHistogram(const std::string& name, Annotations& annotations, Time& universal_time);
		
        /// Adds regex subscription
	void attach(const std::string& regular_expression, ohp::PluginBase& plugin);
    
	/// Removes regex subscription
	void detach(const std::string& regular_expression, ohp::PluginBase& plugin);
        	
        void getHistogramsList(	const std::string & server, 
				const std::string & providers_regex, 
				const std::string & histograms_regex,
                                std::vector<HistogramNameTokens>& histograms) const;
        
	void infrastructureUp();
	
        void infrastructureDown();
        
	TObject* readObject(const std::string& name, Annotations& annotations, Time& universal_time);

        uint32_t getRunNumber() const {
            return m_run_number;
        }

        uint32_t getLumiBlock() const {
            return m_lumi_block;
        }
        
    private:	
        /// This function is called when a histogram is changed on the server.
	void objectChanged(const std::string& hname, const OWLTime &, Reason reason);

	bool watchHistogram(const std::string& name);
        
    private:
    
    	typedef boost::shared_ptr<OHSubscriber> Subscriber;
        
	struct Subscription :	public OHRootReceiver,
				boost::noncopyable
	{
	    Subscription(const IPCPartition& partition, 
			 const std::string& regex, 
			 ohp::PluginBase& plugin,
			 Cache& cache,
                         unsigned int& in_cnt,
                         unsigned int& out_cnt,
                         bool& paused);
	
	    void addPlugin(ohp::PluginBase& plugin);
	  
	    bool removePlugin(const ohp::PluginBase& plugin);
            	  
            void subscribe();
            
	private:	    
            void objectChanged(const std::string& hname, const OWLTime &, Reason reason);

	public:
	    const std::string				m_regex;

	private:
	    mutable boost::mutex			m_mutex;
	    mutable std::vector<ohp::PluginBase*>	m_plugins;
	    Cache&					m_cache;
	    Subscriber					m_subscriber;
	    unsigned int&				m_in_cnt;
	    unsigned int&				m_out_cnt;
	    bool&					m_paused;
            IPCPartition				m_partition;
	};
	
	typedef boost::multi_index_container<
	    boost::shared_ptr<Subscription>,
	    indexed_by<
		hashed_unique <
		    BOOST_MULTI_INDEX_MEMBER(Subscription,const std::string,m_regex)
		>
	    >
	> RegexSubscriptions;

	typedef boost::multi_index_container<
	std::string,
	indexed_by<
	    hashed_unique<identity<std::string> >
	>
	> Subscriptions;
		
    private:
	boost::mutex		m_mutex;
	IPCPartition		m_partition;
	Subscriptions		m_subscriptions;
	Subscriber		m_subscriber;
	RegexSubscriptions	m_regex_subscriptions;
	std::atomic<uint32_t>   m_run_number = -1;
        std::atomic<uint32_t>   m_lumi_block = -1;
        ISInfoWatcher<RunParams> m_rp_watcher;
        ISInfoWatcher<LumiBlock> m_lb_watcher;
    };
}

#endif
