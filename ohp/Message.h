/*
  FILE:		Message.h
  AUTHOR:	andrea.dotti@pi.infn.it
  MODIFIED:	Serguei Kolos
*/
#ifndef OHP_MESSAGE_H
#define OHP_MESSAGE_H

#include <string>
#include <vector>

namespace ohp
{
    class Message
    {
    public:
        enum Type{ InfrastructureDown, InfrastructureUp, 
        	   HistogramUpdated, Paused, Resumed, 
                   ServerDown, ServerUp };
        
	Message(Type type, const std::string& data = std::string())
	  : m_type(type),
	    m_data(data)
	{ ; }

	bool operator==(const Message& msg) const
	{ return m_type == msg.m_type; }
      
	bool operator!=(const Message& msg) const
	{ return !this->operator==(msg); }
      
    private:
	Type		m_type;
	std::string	m_data;
    };
}

#endif
