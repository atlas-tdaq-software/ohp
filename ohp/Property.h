// File:	Property.h
// Info:	Header for data structure Property in the HistogramProperties
// Author:	Davide Carpana [carpana@cli.di.unipi.it]
// MODIFIED:	Serguei Kolos

#ifndef OHP_PROPERTY_H
#define OHP_PROPERTY_H

#include <string>

namespace ohp
{
    struct Property
    {
	Property(const std::string& k, const std::string& v);

	const bool 		m_unique;
	const std::string	m_key;
	mutable std::string	m_value;
    };
}

#endif
  
