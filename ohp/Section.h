/*
  FILE :	Section.h
  AUTHOR:	Serguei Kolos
*/

#ifndef OHP_SECTION_H
#define OHP_SECTION_H

#include <iostream>
#include <string>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/variant/recursive_wrapper.hpp>

#include <ohp/Options.h>

using namespace boost::multi_index;

namespace ohp
{
    /// A Configuration block for the ohp
    struct Section
    {
	Section(const std::string& name)
	  : m_id(name),
            m_name(name)
	{ ; }

	const std::string& getName() const { return m_name; }

	void dump(std::ostream & out) const;

        void replaceVariables(boost::function<std::string (const std::string&)> op) const;

	std::string		m_id;
	mutable std::string	m_name;
	mutable Options		m_options;
        mutable boost::recursive_wrapper<
            boost::multi_index_container<
		Section,
		indexed_by<
		    hashed_unique <
			member<Section,std::string,&Section::m_id>
		    >
		>
	    > 
        > 			m_subsections;
    }; 

    typedef boost::multi_index_container<
	Section,
	indexed_by<
	    hashed_unique <
		member<Section,std::string,&Section::m_id>
	    >
	>
    > Sections; 
}

#endif
