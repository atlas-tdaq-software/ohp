/*
  FILE:         SharedLibrary.h
  AUTHORS:      Paolo Adragna [p.adragna@qmul.ac.uk]
  MODIFIED:	Serguei Kolos
*/

#ifndef SHAREDLIBRARY_H
#define SHAREDLIBRARY_H

#include <dlfcn.h>

#include <string>
#include <ers/ers.h>

ERS_DECLARE_ISSUE ( ohp,
                    SharedLibraryException,
                    "Error with the '" << name << "' shared library: " << error,
                    ((std::string)name )
                    ((std::string)error )
                  )
namespace ohp 
{
    class SharedLibrary
    {
      public:
        SharedLibrary(const std::string & name, int flags = RTLD_NOW|RTLD_GLOBAL);
	
        ~SharedLibrary ();
        
	void* findSymbol(const std::string& name);
        
      private:
        std::string	m_name;
        void*		m_handle;
    };
}

#endif // SHAREDLIBRARY_H
