#ifndef OHP_HISTOGRAM_PROPERTIES_H
#define OHP_HISTOGRAM_PROPERTIES_H

// File:    HistogramProperties.h
// Info:    Header for implementation of a Properties set
// Author:	Davide Carpana [carpana@cli.di.unipi.it]
// MODIFIED:	Serguei Kolos

#include <string>
#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>

#include <ohp/Property.h>

using namespace boost::multi_index;

namespace ohp 
{
    typedef boost::multi_index_container<
	Property,
	indexed_by<
	    hashed_non_unique <
		BOOST_MULTI_INDEX_MEMBER(Property,const std::string,m_key)
	    >
	>
    > PropertiesMap;
    
    /// Data structure to manage a single property for a histogram
    namespace Histogram
    {
	struct Properties
	{
	    /// Creates a Property
	    Properties(const std::string & n) : m_name(n) { ; }

	    Properties(const std::string & n, const std::string& k, const std::string& v);

	    void addProperty(const std::string& k, const std::string& v) const;

	    const std::string & findFirst(const std::string& key) const;

	    std::vector<std::string> findAll(const std::string& key) const;

	    const std::string	  m_name;
	    mutable PropertiesMap m_properties;
	};
    }
    
    typedef boost::multi_index_container<
	Histogram::Properties,
	indexed_by<
	    hashed_unique <
		BOOST_MULTI_INDEX_MEMBER(Histogram::Properties,const std::string,m_name)
	    >
	>
    > HistogramPropertiesBase;

    /// A map of histogram's related properties
    struct HistogramProperties : public HistogramPropertiesBase
    {
      /// Set histograms properties
      void setProperty(	const std::string& histogram,
                        const std::string& key,
                        const std::string& value);
      
      /// Set empty histograms properties
      void setProperty(	const std::string& histogram);
      
      /// Gets the properties associated to an histogram
      /**
       * @param histogram Histogram name
       * @return The set of properties associated to histo if present, otherwise null
       */
      const Histogram::Properties * getProperties(const std::string& histogram) const;
    };
}

#endif
