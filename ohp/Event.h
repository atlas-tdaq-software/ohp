/*
  FILE:		Event.h
  AUTHOR:	Serguei Kolos
*/
#ifndef OHP_EVENT_H
#define OHP_EVENT_H

#include <string>

#include <QEvent>
#include <QString>

namespace ohp
{
    struct Event : public QEvent
    {
	enum Type { HistogramUpdated = QEvent::User,
		    InfrastructureUp, InfrastructureDown,
		    ServerUp, ServerDown, Paused, Resumed };
                    
    	Event(Type type, const std::string& data = std::string()) 
          : QEvent(static_cast<QEvent::Type>(type)),
            m_data(data.c_str())
        { ; }
        
        const QString m_data;
    };
}

#endif
