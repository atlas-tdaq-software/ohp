#ifndef TESTOHCACHE_H_
#define TESTOHCACHE_H_

#include <ohp/cache/OHCache.h>

namespace ohp{
namespace cache{

class TestOHCache : public OHCache{
public:
	TestOHCache(const std::string& filename) : OHCache(filename), m_counter(0){

	}

	CacheElement* sublevel_get(const std::string& key){
		m_counter++;
		return OHCache::sublevel_get(key);
	}

	unsigned int getSublevelCounter(){
		return m_counter;
	}

private:
	unsigned int m_counter;
};

}
}

#endif /* TESTOHCACHE_H_ */
