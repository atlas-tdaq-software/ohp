#include <test/TestTFileCache.h>

#include <TObject.h>

#include <boost/test/unit_test.hpp>

#include <string>

using namespace std;
using namespace ohp::cache;

BOOST_AUTO_TEST_SUITE( TFileCacheSuite )

struct TFileCacheFixture{
	TestTFileCache m_cache;
	static const std::string testHistogram;

	TFileCacheFixture() : m_cache("../test/test.root"){

	}
	~TFileCacheFixture(){}
};

const std::string TFileCacheFixture::testHistogram = "prefix/server1/provider/histogram0";

BOOST_FIXTURE_TEST_CASE( getTest, TFileCacheFixture )
{
	TObject *objA, *objB;
	BOOST_CHECK((objA = m_cache.get(testHistogram)) != NULL);
	BOOST_CHECK(m_cache.size() == 1);
	BOOST_CHECK((objB = m_cache.get(testHistogram)) != NULL);
	BOOST_CHECK(m_cache.size() == 1);
	BOOST_CHECK(m_cache.getSublevelCounter() == 1);
}

BOOST_FIXTURE_TEST_CASE( updateTest, TFileCacheFixture )
{
	TObject *objA, *objB;
	objA = m_cache.get(testHistogram);
	m_cache.updateReceived(testHistogram);
	BOOST_CHECK((objB = m_cache.get(testHistogram)) != NULL);
	BOOST_CHECK(m_cache.size() == 1);
	BOOST_CHECK(m_cache.getSublevelCounter() == 2);
}

BOOST_FIXTURE_TEST_CASE( clearTest, TFileCacheFixture )
{
	m_cache.get(testHistogram);
	m_cache.clear();
	BOOST_CHECK(m_cache.size() == 0);
}

BOOST_AUTO_TEST_SUITE_END()
