#ifndef DUMMY_TFILE_CACHE_H
#define DUMMY_TFILE_CACHE_H

#include <ohp/cache/TFileCache.h>

namespace ohp{
namespace cache{

class TestTFileCache : public TFileCache{
public:
	TestTFileCache(const std::string& filename) : TFileCache(filename, "prefix/"), m_counter(0){

	}

	CacheElement* sublevel_get(const std::string & key){
		m_counter++;
		return TFileCache::sublevel_get(key);
	}

	unsigned int getSublevelCounter(){
		return m_counter;
	}

private:
	unsigned int m_counter;
};

}
}

#endif
