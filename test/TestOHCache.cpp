#include <boost/test/unit_test.hpp>

#include <test/TestOHCache.h>

#include <TObject.h>

using namespace std;
using namespace ohp::cache;

BOOST_AUTO_TEST_SUITE( OHCacheSuite )

struct OHCacheFixture{
	TestOHCache m_cache;

	static const std::string testHistogram;

	OHCacheFixture() : m_cache("be_test"){

	}
};

const std::string OHCacheFixture::testHistogram = "server1/provider/histogram0";

BOOST_FIXTURE_TEST_CASE( getTest, OHCacheFixture )
{
	TObject *objA, *objB;
	BOOST_CHECK((objA = m_cache.get(testHistogram)) != NULL);
	BOOST_CHECK(m_cache.size() == 1);
	BOOST_CHECK((objB = m_cache.get(testHistogram)) != NULL);
	BOOST_CHECK(m_cache.size() == 1);
	BOOST_CHECK(m_cache.getSublevelCounter() == 1);
}

BOOST_FIXTURE_TEST_CASE( updateTest, OHCacheFixture )
{
	TObject *objA, *objB;
	objA = m_cache.get(testHistogram);
	m_cache.updateReceived(testHistogram);
	BOOST_CHECK((objB = m_cache.get(testHistogram)) != NULL);
	BOOST_CHECK(m_cache.size() == 1);
	BOOST_CHECK(m_cache.getSublevelCounter() == 2);
}

BOOST_FIXTURE_TEST_CASE( clearTest, OHCacheFixture )
{
	m_cache.get(testHistogram);
	m_cache.clear();
	BOOST_CHECK(m_cache.size() == 0);
}

BOOST_AUTO_TEST_SUITE_END()
