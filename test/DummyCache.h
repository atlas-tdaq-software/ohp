#ifndef DUMMYCACHE_H_
#define DUMMYCACHE_H_

#include <ohp/cache/HistogramCache.h>

namespace ohp{
namespace cache{

class DummyCache : public HistogramCache{
public:
	virtual CacheElement* sublevel_get(const std::string & key){
		return NULL;
	}

	virtual void del_policy(const std::string & key, CacheElement* value){}
};

}
}

#endif /* DUMMYCACHE_H_ */
